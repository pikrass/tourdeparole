use crux_core::{render::Render, App};
use crux_macros::Effect;
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;

use crate::capabilities::{Datetime, Random, Storage};
use crate::nav::{NavState, Screen, ScreenCat, ProfileScreen, SessionScreen, HistoryScreen};
use crate::nav::{ProfileScreenView, SessionScreenView, HistoryScreenView};
use crate::ordering::{OrderRule, OrderRuleData, all_rules_data};
use crate::profile::Profile;
use crate::session::{Session, SessionIndexEntry, SessionStats, SessionView};
use crate::storage::{deser_profiles, deser_session_index, deser_session};
use crate::storage::{ser_profiles, ser_session_index, ser_session};


#[derive(Default)]
pub struct Model {
    profiles: Vec<Profile>,
    sessions: VecDeque<SessionIndexEntry>,
    active_session: Option<Session>,
    stats_session: StatsSession,
    nav_state: NavState,
}

#[derive(Default)]
pub enum StatsSession {
    #[default]
    None,
    Active,
    Other(Session),
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ViewModel {
    pub profiles: Vec<Profile>,
    pub sessions: Vec<SessionIndexEntry>,
    pub available_order_rules: Vec<OrderRuleData>,
    pub active_session: Option<SessionView>,
    pub screen: Screen,
    pub show_back_button: bool,
}


#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum Event {
    Start,
    Stop,
    InitProfiles(Option<String>),
    InitSessions(Option<String>),

    NavCat(ScreenCat),
    NavBack,
    NavEditProfile(u64),
    NavEditMembers,
    NavSessionStats(u64),

    NewProfile,
    CreateProfile { id: u64, nav: bool },
    CloneProfile{ original: u64 },
    DoCloneProfile{ original: u64, id: u64 },
    DeleteProfile(u64),

    EditProfileName{ id: u64, name: String },
    EditProfileTimelimitFirst{ id: u64, value: u64 },
    EditProfileTimelimitNext{ id: u64, value: u64 },
    AddOrderRule{ id: u64, rule: OrderRule },
    RemoveOrderRule{ id: u64, rule: OrderRule },
    MoveOrderRule{ id: u64, rule: OrderRule, delta: isize },
    ToggleResetTimelimit{ id: u64 },
    ToggleResetSpeeches{ id: u64 },
    ToggleResetTimer{ id: u64 },
    ToggleResetStats{ id: u64 },

    OpenSession(Option<u64>),
    LoadSession(Option<String>),
    NewSession(Option<u64>),
    CreateSession(u64, u64),
    DeleteSession(u64),

    CreateMember(String, bool),
    DeleteMember(usize),
    ToggleMemberPrio(usize),
    ToggleMemberHidden(usize),

    ChangeSessionProfile(u64),
    NewSubsession,
    ToggleAskToSpeak(usize),
    AskToSpeak(usize, u64),
    NextSpeaker,
    ForceSpeaker(usize),
    DoForceSpeaker(usize, u64),

    StartTimer,
    InitTimer(u64),
    Tick(u64),
    StopTimer,

    UpdateSessionTimes(i64, i32),
    EditSessionName{ id: u64, name: String },
    LoadSessionForStats(Option<String>),
}

#[cfg_attr(feature = "typegen", derive(crux_macros::Export))]
#[derive(Effect)]
#[effect(app = "Tdp")]
pub struct Capabilities {
    datetime: Datetime<Event>,
    random: Random<Event>,
    render: Render<Event>,
    storage: Storage<Event>,
}

#[derive(Default)]
pub struct Tdp;


const PROFILES_FILE: &str = "profiles.ron";
const SESSION_INDEX_FILE: &str = "sessions.ron";
macro_rules! session_filename {
    ($id:expr) => {&format!("session-{}.ron", $id)}
}

impl App for Tdp {
    type Event = Event;
    type Model = Model;
    type ViewModel = ViewModel;
    type Capabilities = Capabilities;

    fn update(&self, event: Self::Event, model: &mut Self::Model, caps: &Self::Capabilities) {
        match event {
            Event::Start => {
                caps.storage.retrieve(PROFILES_FILE, Event::InitProfiles);
                caps.storage.retrieve(SESSION_INDEX_FILE, Event::InitSessions);
            }

            Event::Stop => {
                caps.storage.replace(PROFILES_FILE, ser_profiles(&model.profiles));
                caps.storage.replace(SESSION_INDEX_FILE, ser_session_index(&model.sessions));

                if let Some(session) = model.active_session.as_ref() {
                    caps.storage.replace(
                        session_filename!(session.id()),
                        ser_session(session)
                    );
                }
            }

            Event::InitProfiles(profiles) => {
                if None == profiles {
                    caps.random.random(|id| Event::CreateProfile { id, nav: false });
                } else if let Some(content) = profiles {
                    let deser = deser_profiles(&content);

                    match deser {
                        Ok(profs) if profs.len() > 0 => {
                            model.profiles = profs;
                            caps.render.render();
                        }
                        _ => {
                            caps.random.random(|id| Event::CreateProfile { id, nav: false });
                        }
                    }
                }
            }

            Event::InitSessions(sessions) => {
                if let Some(content) = sessions {
                    let deser = deser_session_index(&content);

                    if let Ok(sess) = deser {
                        model.sessions = sess;
                        caps.render.render();
                    }
                }
            }


            // ------ Navigation ------

            Event::NavCat(cat) => {
                model.nav_state.nav_cat(cat);

                if cat == ScreenCat::Session && model.active_session.is_none() {
                    self.update(Event::OpenSession(None), model, caps);
                }

                caps.render.render();
            }

            Event::NavBack => {
                model.nav_state.back();
                caps.render.render();
            }

            Event::NavEditProfile(id) => {
                if model.profiles.iter().any(|p| p.id == id) {
                    model.nav_state.nav_profile(ProfileScreen::EditProfile(id));
                    caps.render.render();
                }
            }

            Event::NavEditMembers => {
                model.nav_state.nav_session(SessionScreen::EditMembers);
                caps.render.render();
            }

            Event::NavSessionStats(id) => {
                if model.active_session.as_ref().is_some_and(|s| s.id() == id) {
                    model.stats_session = StatsSession::Active;
                    model.nav_state.nav_history(HistoryScreen::SessionOverview);
                    caps.render.render();
                } else if model.sessions.iter().any(|s| s.id == id) {
                    caps.storage.retrieve(
                        session_filename!(id),
                        Event::LoadSessionForStats
                    );
                }
            }


            // ------ Profiles ------

            Event::NewProfile => {
                caps.random.random(|id| Event::CreateProfile { id, nav: true });
            }

            Event::CreateProfile { id, nav } => {
                let mut profile = Profile::new(id);

                while model.profiles.iter().any(|p| p.name == profile.name) {
                    profile.name = increment_name(&profile.name);
                }

                model.profiles.push(profile);

                if nav {
                    model.nav_state.nav_profile(ProfileScreen::EditProfile(id));
                }

                caps.render.render();
            }

            Event::CloneProfile { original } => {
                caps.random.random(move |id| Event::DoCloneProfile { original, id } );
            }

            Event::DoCloneProfile { original, id } => {
                if let Some(original) = model.profiles.iter().find(|p| p.id == original) {
                    let mut profile = original.clone();
                    profile.id = id;
                    profile.name = increment_name(&profile.name);
                    model.profiles.push(profile);
                    model.nav_state.nav_profile(ProfileScreen::EditProfile(id));
                    caps.render.render();
                }
            }

            Event::DeleteProfile(id) => {
                let res = model.profiles.iter_mut().position(|p| p.id == id);
                if let Some(index) = res {
                    model.profiles.remove(index);
                    model.nav_state.nav_profile(ProfileScreen::ProfileList);
                    caps.render.render();
                }
            }

            Event::EditProfileName { id, name } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.name = name;
                    caps.render.render();
                }
            }

            Event::EditProfileTimelimitFirst { id, value } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.timelimit_first = value;
                    caps.render.render();
                }
            }

            Event::EditProfileTimelimitNext { id, value } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.timelimit_next = value;
                    caps.render.render();
                }
            }

            Event::AddOrderRule { id, rule } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.add_rule(rule);
                    caps.render.render();
                }
            }

            Event::RemoveOrderRule { id, rule } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.remove_rule(rule);
                    caps.render.render();
                }
            }

            Event::MoveOrderRule { id, rule, delta } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.move_rule(rule, delta);
                    caps.render.render();
                }
            }

            Event::ToggleResetTimelimit { id } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.reset_timelimit = !profile.reset_timelimit;
                    caps.render.render();
                }
            }

            Event::ToggleResetSpeeches { id } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.reset_speeches = !profile.reset_speeches;
                    caps.render.render();
                }
            }

            Event::ToggleResetTimer { id } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.reset_timer = !profile.reset_timer;
                    caps.render.render();
                }
            }

            Event::ToggleResetStats { id } => {
                let res = model.profiles.iter_mut().find(|p| p.id == id);
                if let Some(profile) = res {
                    profile.reset_stats = !profile.reset_stats;
                    caps.render.render();
                }
            }


            // ------ Session management ------

            Event::OpenSession(id) => {
                let sess_index = id.map_or_else(
                    || model.sessions.get(0),
                    |id| model.sessions.iter().find(|s| s.id == id));

                if let Some(sess_index) = sess_index {
                    if model.stats_session.loaded_id(sess_index.id) {
                        // The session was already loaded for statistics: just take it
                        unload_session(model, caps);
                        model.active_session = model.stats_session.take_for_active();
                        caps.render.render();
                    } else {
                        caps.storage.retrieve(
                            session_filename!(sess_index.id),
                            Event::LoadSession
                        );
                    }
                } else if let Some(profile) = model.profiles.get(0) {
                    let prof_id = profile.id.clone();
                    caps.random.random(move |id| Event::CreateSession(id, prof_id));
                }
            }

            Event::LoadSession(content) => {
                unload_session(model, caps);

                if let Some(content) = content {
                    let deser = deser_session(&content);

                    if let Ok(sess) = deser {
                        if model.stats_session.loaded_id(sess.id()) {
                            // The session was already loaded for statistics: just take it
                            model.active_session = model.stats_session.take_for_active();
                        } else {
                            model.active_session = Some(sess);
                            model.update_session_index();
                            model.nav_state.nav_cat(ScreenCat::Session);
                        }
                        caps.render.render();
                    }
                }
            }

            Event::NewSession(profile_id) => {
                let profile = profile_id
                    .and_then(|i| model.profiles.iter().find(|p| p.id == i))
                    .or_else(|| model.profiles.get(0));

                if let Some(prof) = profile {
                    let prof_id = prof.id.clone();
                    caps.random.random(move |id| Event::CreateSession(id, prof_id));
                }
            }

            Event::CreateSession(id, profile_id) => {
                unload_session(model, caps);

                model.active_session = Some(Session::new(id, profile_id));
                model.update_session_index();
                model.nav_state.nav_cat(ScreenCat::Session);
                caps.render.render();
            }

            Event::DeleteSession(id) => {
                let pos = model.sessions.iter().position(|s| s.id == id);
                if let Some(pos) = pos {
                    model.sessions.remove(pos);
                    caps.storage.delete(&format!("session-{}.ron", id));
                }

                // Check if it was the active session.
                if let Some(session) = model.active_session.take() {
                    if session.id() == id {
                        // If so, turn it to None, create a new one if we're on the session tab,
                        // and fix the reference for stats_session if needed.
                        if model.nav_state.screen_cat() == ScreenCat::Session {
                            self.update(Event::OpenSession(None), model, caps);
                        }
                        match &model.stats_session {
                            StatsSession::Active => model.stats_session = StatsSession::None,
                            StatsSession::None => (),
                            StatsSession::Other(_) => (),
                        }
                    } else {
                        // If not, put it back.
                        model.active_session = Some(session);
                    }
                }

                caps.render.render();
            }


            // ------ Active session ------

            // Members management

            Event::CreateMember(name, prio) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.create_member(name.trim().to_owned(), prio);
                    caps.render.render();
                }
            }

            Event::DeleteMember(id) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.delete_member(id);
                    caps.render.render();
                }
            }

            Event::ToggleMemberPrio(id) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.toggle_member_prio(id);
                    caps.render.render();
                }
            }

            Event::ToggleMemberHidden(id) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.toggle_member_hidden(id);
                    caps.render.render();
                }
            }


            // Actions

            Event::ChangeSessionProfile(profile_id) => {
                if model.profiles.iter().any(|p| p.id == profile_id) {
                    if let Some(ref mut session) = model.active_session.as_mut() {
                        session.set_profile(profile_id);
                        caps.render.render();
                    }
                }
            }

            Event::NewSubsession => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.next_subsession();
                    caps.render.render();
                }
            }

            Event::ToggleAskToSpeak(id) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    let status = session.wants_to_speak(id);
                    if let Some(s) = status {
                        if s {
                            session.cancel_ask_to_speak(id);

                            // Stop the timer in case they were the active speaker
                            if session.active_speaker().is_none() {
                                caps.datetime.stop_ticking();
                            }

                            caps.render.render();
                        } else {
                            caps.datetime.instant(move |time| Event::AskToSpeak(id, time));
                        }
                    }
                }
            }

            Event::AskToSpeak(id, time) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.ask_to_speak(id, time);
                    caps.render.render();
                }
            }

            Event::NextSpeaker => {
                if let Some(mut session) = model.active_session.take() {
                    let default = Profile::new(4242);
                    let profile = model.get_session_profile(&session, &default);
                    session.next(profile);
                    model.active_session = Some(session);
                    caps.datetime.stop_ticking();
                    caps.datetime.local_time(Event::UpdateSessionTimes);
                    caps.render.render();
                }
            }

            Event::ForceSpeaker(id) => {
                caps.datetime.instant(move |time| Event::DoForceSpeaker(id, time));
            }

            Event::DoForceSpeaker(id, time) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.force_speaker(id, time);
                    caps.datetime.stop_ticking();
                    caps.datetime.local_time(Event::UpdateSessionTimes);
                    caps.render.render();
                }
            }

            Event::EditSessionName { id, name } => {
                if let Some(entry) = model.sessions.iter_mut().find(|s| s.id == id) {
                    entry.name = name;
                    caps.render.render();
                }
            }

            // Timer

            Event::StartTimer => {
                if model.active_session.is_some() {
                    caps.datetime.instant(|time| Event::InitTimer(time));
                }
            }

            Event::InitTimer(time) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.start_timer(time);
                    caps.datetime.start_ticking(250, |t| Event::Tick(t));
                    caps.render.render();
                }
            }

            Event::Tick(time) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.tick(time);
                    caps.render.render();
                }
            }

            Event::StopTimer => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.stop_timer();
                    caps.datetime.stop_ticking();
                    caps.datetime.local_time(Event::UpdateSessionTimes);
                }
            }

            // History

            Event::UpdateSessionTimes(datetime, offset) => {
                if let Some(ref mut session) = model.active_session.as_mut() {
                    session.update_times(datetime, offset);
                    model.update_session_index();
                }
            }

            Event::LoadSessionForStats(content) => {
                if let Some(content) = content {
                    let deser = deser_session(&content);

                    if let Ok(sess) = deser {
                        if model.active_session.as_ref().is_some_and(|s| s.id() == sess.id()) {
                            model.stats_session = StatsSession::Active;
                        } else {
                            model.stats_session = StatsSession::Other(sess);
                        }
                        model.nav_state.nav_history(HistoryScreen::SessionOverview);
                        caps.render.render();
                    }
                }
            }
        };
    }

    fn view(&self, model: &Self::Model) -> Self::ViewModel {
        let mut profile: Option<&Profile> = None;
        let default_profile = Profile::new(4242);

        if let Some(session) = model.active_session.as_ref() {
            profile = Some(model.get_session_profile(session, &default_profile));
        }

        let screen = match model.nav_state.screen_cat() {
            ScreenCat::Profile => Screen::Profile(match model.nav_state.profile_screen() {
                ProfileScreen::ProfileList => ProfileScreenView::ProfileList,
                ProfileScreen::EditProfile(id) => ProfileScreenView::EditProfile(id),
            }),

            ScreenCat::Session => Screen::Session(match model.nav_state.session_screen() {
                SessionScreen::ActiveSession => SessionScreenView::ActiveSession,
                SessionScreen::EditMembers => SessionScreenView::EditMembers,
            }),

            ScreenCat::History => Screen::History(match model.nav_state.history_screen() {
                HistoryScreen::SessionList => HistoryScreenView::SessionList,
                HistoryScreen::SessionOverview => model.session_stats()
                    .map(|s| HistoryScreenView::SessionOverview(s))
                    .unwrap_or(HistoryScreenView::SessionList),
            }),
        };

        let view = ViewModel {
            profiles: model.profiles.clone(),
            sessions: model.sessions.clone().into(),
            available_order_rules: all_rules_data(),
            active_session: model.active_session.as_ref().map(|s| s.view(profile.unwrap())),

            screen,
            show_back_button: model.nav_state.show_back_button(),
        };

        view
    }
}

impl Model {
    pub fn get_session_profile<'a, 'b>(&'a self, session: &'b Session, default: &'a Profile)
        -> &'a Profile
    {
        match self.profiles.iter().find(|p| p.id == session.profile()) {
            Some(profile) => profile,
            None => {
                // Something went wrong, the profile disappeared. Just use the first one.
                if let Some(p) = self.profiles.get(0) {
                    p
                } else {
                    // There is no profile at all, return the default one
                    default
                }
            }
        }
    }

    pub fn update_session_index(&mut self) {
        if let Some(session) = self.active_session.as_ref() {
            let prev_index = self.sessions.iter().position(|s| s.id == session.id());
            let mut new_entry = session.index_entry();
            if let Some(prev_index) = prev_index {
                let prev_entry = self.sessions.remove(prev_index).unwrap();
                new_entry.name = prev_entry.name;
            }
            self.sessions.push_front(new_entry);
        }
    }

    fn session_stats(&self) -> Option<SessionStats> {
        match &self.stats_session {
            StatsSession::None => None,
            StatsSession::Active => self.active_session.as_ref().map(|s| s.stats()),
            StatsSession::Other(session) => Some(session.stats()),
        }
    }
}

impl StatsSession {
    fn loaded_id(&self, id: u64) -> bool {
        match self {
            Self::None => false,
            Self::Active => false,
            Self::Other(ref sess) => sess.id() == id,
        }
    }

    fn take_for_active(&mut self) -> Option<Session> {
        let taken = std::mem::replace(self, StatsSession::Active);
        match taken {
            Self::None => { *self = StatsSession::None; None },
            Self::Active => { *self = StatsSession::None; None },
            Self::Other(sess) => Some(sess),
        }
    }
}

fn increment_name(name: &str) -> String {
    let cur_number = name
        .chars()
        .rev()
        .take_while(|c| c.is_ascii_digit())
        .enumerate()
        .try_fold(0, |num, (pos, c)|
            Some(num + 10u32.pow(pos.try_into().ok()?) * c.to_digit(10)?))
        .unwrap_or(0);

    let next_number = if cur_number == 0 {
        2
    } else {
        cur_number + 1
    };

    return name
        .chars()
        .rev()
        .skip_while(|c| c.is_ascii_digit())
        .collect::<String>()
        .trim()
        .chars()
        .rev()
        .collect::<String>() + " " + &next_number.to_string();
}

fn unload_session(model: &mut Model, caps: &<Tdp as App>::Capabilities) {
    if let Some(mut session) = model.active_session.take() {
        session.stop_timer();

        caps.storage.replace(
            session_filename!(session.id()),
            ser_session(&session)
        );

        match &model.stats_session {
            StatsSession::Active => model.stats_session = StatsSession::Other(session),
            StatsSession::None => (),
            StatsSession::Other(_) => (),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use assert_let_bind::assert_let;
    use crux_core::assert_effect;
    use crux_core::testing::AppTester;

    use crate::capabilities::storage::StorageOperation;
    use crate::capabilities::random::RandomOutput;

    #[test]
    fn start_creates_a_default_profile_if_empty() {
        let tdp = AppTester::<Tdp, _>::default();
        let mut model = Model::default();

        let update = tdp.update(Event::Start, &mut model);

        assert_let!(Effect::Storage(action), &update.effects[0]);

        let expected = StorageOperation::Retrieve{filename: String::from(PROFILES_FILE)};
        assert_eq!(&action.operation, &expected);

        let update = tdp.update(Event::InitProfiles(None), &mut model);

        assert_effect!(update, Effect::Random(_));

        let update = tdp.update(Event::CreateProfile { id: 42, nav: false }, &mut model);

        assert_effect!(update, Effect::Render(_));
        assert_eq!(tdp.view(&model).profiles.len(), 1);
        assert_eq!(tdp.view(&model).profiles[0].name, String::from("Défaut"));
    }

    #[test]
    fn new_session_uses_the_first_profile_by_default() {
        let tdp = AppTester::<Tdp, _>::default();
        let mut model = Model::default();
        tdp.update(Event::Start, &mut model);
        tdp.update(Event::InitProfiles(None), &mut model);
        tdp.update(Event::CreateProfile { id: 42, nav: false }, &mut model);

        let mut effects = tdp.update(Event::NewSession(None), &mut model).into_effects();
        assert_let!(Effect::Random(mut req), effects.next().unwrap());

        let update = tdp.resolve(&mut req, RandomOutput(1420));
        assert_eq!(update.unwrap().events[0], Event::CreateSession(1420, 42));
    }

    #[test]
    fn increment_name_adds_a_two_if_no_number() {
        assert_eq!(increment_name("Pouet"), "Pouet 2");
    }

    #[test]
    fn increment_name_increments_the_last_num() {
        assert_eq!(increment_name("Pouet 3"), "Pouet 4");
        assert_eq!(increment_name("Pouet 99"), "Pouet 100");
    }

    #[test]
    fn increment_name_handles_round_numbers_correctly() {
        assert_eq!(increment_name("Pouet 10"), "Pouet 11");
    }

    #[test]
    fn prio_alternate_ordering_stays_consistent() {
        use crate::ordering::OrderRule;
        use crate::profile::Profile;
        use crate::session::Session;

        let mut profile = Profile::new(0);
        profile.clear_rules();
        profile.add_rule(OrderRule::PrioAlternate);

        let mut session = Session::new(0, profile.id);
        session.create_member(String::from("non-prio 1"), false);
        session.create_member(String::from("non-prio 2"), false);
        session.create_member(String::from("prio"), true);

        // Create a queue so that np1 and np2 ask first, and prio should get scheduled between them
        session.ask_to_speak(0, 0);
        session.ask_to_speak(1, 1);
        session.ask_to_speak(2, 2);

        // Let the first member speak, then check prio is still scheduled
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("non-prio 1"));
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("prio"));
    }

    #[test]
    fn prio_alternate_ordering_resets_if_speeches_are_reset() {
        use crate::ordering::OrderRule;
        use crate::profile::Profile;
        use crate::session::Session;

        let mut profile = Profile::new(0);
        profile.reset_speeches = true;
        profile.clear_rules();
        profile.add_rule(OrderRule::PrioAlternate);

        let mut session = Session::new(0, profile.id);
        session.create_member(String::from("non-prio 1"), false);
        session.create_member(String::from("non-prio 2"), false);
        session.create_member(String::from("prio"), true);

        // Create a queue so that np1 and np2 ask first, and prio should get scheduled between them
        session.ask_to_speak(0, 0);
        session.ask_to_speak(1, 1);
        session.ask_to_speak(2, 2);

        // Let the first member speak, reset, then check prio is not scheduled next
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("non-prio 1"));
        session.next_subsession();
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("non-prio 2"));
    }

    #[test]
    fn prio_alternate_ordering_stays_consistent_across_subsessions_if_speeches_dont_reset() {
        use crate::ordering::OrderRule;
        use crate::profile::Profile;
        use crate::session::Session;

        let mut profile = Profile::new(0);
        profile.reset_speeches = false;
        profile.clear_rules();
        profile.add_rule(OrderRule::PrioAlternate);

        let mut session = Session::new(0, profile.id);
        session.create_member(String::from("non-prio 1"), false);
        session.create_member(String::from("non-prio 2"), false);
        session.create_member(String::from("prio"), true);

        // Create a queue so that np1 and np2 ask first, and prio should get scheduled between them
        session.ask_to_speak(0, 0);
        session.ask_to_speak(1, 1);
        session.ask_to_speak(2, 2);

        // Let the first member speak, reset, then check prio is still scheduled
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("non-prio 1"));
        session.next_subsession();
        session.next(&profile);
        assert_eq!(session.active_speaker().map(|m| m.name()), Some("prio"));
    }
}
