use crux_core::capability::CapabilityContext;
use crux_core::capability::Operation;
use crux_macros::Capability;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct RandomOperation;

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct RandomOutput(pub u64);

impl Operation for RandomOperation {
    type Output = RandomOutput;
}

#[derive(Capability)]
pub struct Random<Ev> {
    context: CapabilityContext<RandomOperation, Ev>,
}

impl<Ev> Random<Ev>
where
    Ev: 'static
{
    pub fn new(context: CapabilityContext<RandomOperation, Ev>) -> Self {
        Self { context }
    }

    pub fn random<F>(&self, event: F)
    where
        F: Fn(u64) -> Ev + Send + 'static,
    {
        let ctx = self.context.clone();

        self.context.spawn(async move {
            let output = ctx.request_from_shell(RandomOperation).await;
            ctx.update_app(event(output.0));
        });
    }
}
