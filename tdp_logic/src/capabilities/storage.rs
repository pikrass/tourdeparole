use crux_core::capability::CapabilityContext;
use crux_core::capability::Operation;
use crux_macros::Capability;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum StorageOperation {
    Replace { filename: String, content: String },
    Append { filename: String, content: String },
    Retrieve { filename: String },
    Delete { filename: String },
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct StorageOutput(Option<String>);

impl Operation for StorageOperation {
    type Output = StorageOutput;
}

#[derive(Capability)]
pub struct Storage<Ev> {
    context: CapabilityContext<StorageOperation, Ev>,
}

impl<Ev> Storage<Ev>
where
    Ev: 'static
{
    pub fn new(context: CapabilityContext<StorageOperation, Ev>) -> Self {
        Self { context }
    }

    pub fn replace(&self, filename: &str, content: String) {
        let ctx = self.context.clone();
        let filename = String::from(filename);

        self.context.spawn(async move {
            ctx.notify_shell(StorageOperation::Replace{filename, content}).await;
        });
    }

    pub fn append(&self, filename: &str, content: String) {
        let ctx = self.context.clone();
        let filename = String::from(filename);

        self.context.spawn(async move {
            ctx.notify_shell(StorageOperation::Append{filename, content}).await;
        });
    }

    pub fn retrieve<F>(&self, filename: &str, event: F)
    where
        F: Fn(Option<String>) -> Ev + Send + 'static,
    {
        let ctx = self.context.clone();
        let filename = String::from(filename);

        self.context.spawn(async move {
            let output = ctx.request_from_shell(StorageOperation::Retrieve{filename}).await;
            ctx.update_app(event(output.0));
        });
    }

    pub fn delete(&self, filename: &str) {
        let ctx = self.context.clone();
        let filename = String::from(filename);

        self.context.spawn(async move {
            ctx.notify_shell(StorageOperation::Delete{filename}).await;
        });
    }
}
