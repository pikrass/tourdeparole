use crux_core::capability::CapabilityContext;
use crux_core::capability::Operation;
use crux_macros::Capability;
use futures::StreamExt;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum DatetimeOperation {
    Instant,
    LocalTime,
    StartTicking(u64),
    StopTicking,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub enum DatetimeOutput {
    Instant(u64),
    LocalTime(i64, i32),
}

impl Operation for DatetimeOperation {
    type Output = DatetimeOutput;
}

#[derive(Capability)]
pub struct Datetime<Ev> {
    context: CapabilityContext<DatetimeOperation, Ev>,
}

impl<Ev> Datetime<Ev>
where
    Ev: 'static
{
    pub fn new(context: CapabilityContext<DatetimeOperation, Ev>) -> Self {
        Self { context }
    }

    pub fn instant<F>(&self, event: F)
    where
        F: Fn(u64) -> Ev + Send + 'static,
    {
        let ctx = self.context.clone();

        self.context.spawn(async move {
            let output = ctx.request_from_shell(DatetimeOperation::Instant).await;
            if let DatetimeOutput::Instant(instant) = output {
                ctx.update_app(event(instant));
            }
        });
    }

    pub fn local_time<F>(&self, event: F)
    where
        F: Fn(i64, i32) -> Ev + Send + 'static,
    {
        let ctx = self.context.clone();

        self.context.spawn(async move {
            let output = ctx.request_from_shell(DatetimeOperation::LocalTime).await;
            if let DatetimeOutput::LocalTime(instant, tz) = output {
                ctx.update_app(event(instant, tz));
            }
        });
    }

    pub fn start_ticking<F>(&self, every: u64, event: F)
    where
        F: Fn(u64) -> Ev + Send + 'static,
    {
        let ctx = self.context.clone();

        self.context.spawn(async move {
            let mut stream = ctx.stream_from_shell(DatetimeOperation::StartTicking(every));

            while let Some(output) = stream.next().await {
                if let DatetimeOutput::Instant(instant) = output {
                    ctx.update_app(event(instant));
                }
            }
        });
    }

    pub fn stop_ticking(&self) {
        let ctx = self.context.clone();

        self.context.spawn(async move {
            ctx.notify_shell(DatetimeOperation::StopTicking).await;
        });
    }
}
