use serde::{Deserialize, Serialize};

use crate::session::SessionStats;

#[derive(Serialize, Deserialize, Clone)]
pub enum Screen {
    Profile(ProfileScreenView),
    Session(SessionScreenView),
    History(HistoryScreenView),
}

#[derive(Default, Serialize, Deserialize, Clone, Copy, PartialEq, Eq, Debug)]
pub enum ScreenCat {
    #[default]
    Profile,
    Session,
    History,
}

#[derive(Default, Clone, Copy, PartialEq)]
pub enum ProfileScreen {
    #[default]
    ProfileList,
    EditProfile(u64),
}

#[derive(Default, Clone, Copy, PartialEq)]
pub enum SessionScreen {
    #[default]
    ActiveSession,
    EditMembers,
}

#[derive(Default, Clone, Copy, PartialEq)]
pub enum HistoryScreen {
    #[default]
    SessionList,
    SessionOverview,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub enum ProfileScreenView {
    ProfileList,
    EditProfile(u64),
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub enum SessionScreenView {
    ActiveSession,
    EditMembers,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub enum HistoryScreenView {
    SessionList,
    SessionOverview(SessionStats),
}


#[derive(Default)]
pub struct NavState {
    screen_cat: ScreenCat,

    profile_screen: ProfileScreen,
    session_screen: SessionScreen,
    history_screen: HistoryScreen,
}

impl NavState {
    pub fn show_back_button(&self) -> bool {
        match self.screen_cat {
            ScreenCat::Profile => self.profile_screen != Default::default(),
            ScreenCat::Session => self.session_screen != Default::default(),
            ScreenCat::History => self.history_screen != Default::default(),
        }
    }

    pub fn screen_cat(&self) -> ScreenCat {
        self.screen_cat
    }

    pub fn profile_screen(&self) -> ProfileScreen {
        self.profile_screen
    }

    pub fn session_screen(&self) -> SessionScreen {
        self.session_screen
    }

    pub fn history_screen(&self) -> HistoryScreen {
        self.history_screen
    }

    pub fn nav_cat(&mut self, cat: ScreenCat) {
        self.screen_cat = cat;
    }

    pub fn nav_profile(&mut self, screen: ProfileScreen) {
        self.profile_screen = screen;
    }

    pub fn nav_session(&mut self, screen: SessionScreen) {
        self.session_screen = screen;
    }

    pub fn nav_history(&mut self, screen: HistoryScreen) {
        self.history_screen = screen;
    }

    pub fn back(&mut self) {
        match self.screen_cat {
            ScreenCat::Profile => self.profile_screen = Default::default(),
            ScreenCat::Session => self.session_screen = Default::default(),
            ScreenCat::History => self.history_screen = Default::default(),
        }
    }
}
