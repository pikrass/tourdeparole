use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::VecDeque;
use std::hash::Hash;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

use crate::session::MemberStatsHandle;

#[derive(Serialize, Deserialize, Clone, PartialEq, Eq, Debug, EnumIter)]
pub enum OrderRule {
    FirstAsked,
    SpeechCount,
    TimeUsed,
    PrioFirst,
    PrioAlternate,
    /* Future
    OldestSpeech,
    PrioFirstIfLessSpeeches,
    PrioFirstIfLessTime,
    */
}

#[derive(Serialize, Deserialize, Clone)]
pub struct OrderRuleData {
    pub rule: OrderRule,
    pub desc: String,
    pub must_be_last: bool,
}

pub struct Context {
    pub last_speaker_nonprio: bool,
}

struct Bucket<'a, T>
where T: Eq + Hash + Copy
{
    key: T,
    mem: Vec<MemberStatsHandle<'a>>,
}

impl OrderRule {
    pub fn must_be_last(&self) -> bool {
        match self {
            OrderRule::FirstAsked => true,
            OrderRule::SpeechCount => false,
            OrderRule::TimeUsed => false,
            OrderRule::PrioFirst => false,
            OrderRule::PrioAlternate => false,
        }
    }

    pub fn data(&self) -> OrderRuleData {
        OrderRuleData {
            rule: self.clone(),
            must_be_last: self.must_be_last(),
            desc: String::from(match self {
                OrderRule::FirstAsked => "Ordre des demandes",
                OrderRule::SpeechCount => "Qui a le moins d'interventions",
                OrderRule::TimeUsed => "Qui a parlé le moins longtemps",
                OrderRule::PrioFirst => "Les personnes prio passent avant",
                OrderRule::PrioAlternate => "Les personnes prio ont un tour sur deux",
            })
        }
    }
}

pub fn all_rules_data() -> Vec<OrderRuleData> {
    let mut vec = Vec::new();

    for rule in OrderRule::iter() {
        vec.push(rule.data());
    }

    vec
}

pub fn order<'a, 'b>(
    members: Vec<MemberStatsHandle<'a>>,
    rules: &'b Vec<OrderRule>,
    context: &Context,
) -> Vec<MemberStatsHandle<'a>> {
    do_order(members, rules, context, 0)
}

fn do_order<'a, 'b>(
    members: Vec<MemberStatsHandle<'a>>,
    rules: &'b Vec<OrderRule>,
    context: &Context,
    index: usize,
) -> Vec<MemberStatsHandle<'a>> {
    let mut members = members;

    match rules[index] {
        OrderRule::FirstAsked => {
            members.sort_by(|a, b| {
                a.time_asked_to_speak().unwrap().cmp(&b.time_asked_to_speak().unwrap())
            });
            members
        }

        OrderRule::SpeechCount => {
            let buckets = filter_into_buckets(members, |m| m.speeches());
            let buckets = sort_inside_buckets(buckets, rules, context, index + 1);
            sort_and_reduce_buckets(buckets)
        }

        OrderRule::TimeUsed => {
            let buckets = filter_into_buckets(members, |m| m.timer());
            let buckets = sort_inside_buckets(buckets, rules, context, index + 1);
            sort_and_reduce_buckets(buckets)
        }

        OrderRule::PrioFirst => {
            let buckets = filter_into_buckets(members, |m| !m.prio());
            let buckets = sort_inside_buckets(buckets, rules, context, index + 1);
            sort_and_reduce_buckets(buckets)
        }

        OrderRule::PrioAlternate => {
            let mut rem: VecDeque<MemberStatsHandle> =
                do_order(members, rules, context, index + 1).into_iter().collect();
            let mut members = Vec::new();
            let mut last_one_was_nonprio = context.last_speaker_nonprio;
            let mut no_more_prio = false;

            while !no_more_prio {
                if let Some(mem) = rem.pop_front() {
                    if mem.prio() {
                        last_one_was_nonprio = false;
                    } else {
                        if last_one_was_nonprio {
                            // Bring a prio member up
                            if let Some(prio_mem_i) = rem.iter().position(|m| m.prio()) {
                                members.push(rem.remove(prio_mem_i).unwrap());
                            } else {
                                no_more_prio = true;
                            }
                        } else {
                            last_one_was_nonprio = true;
                        }
                    }
                    members.push(mem);
                } else {
                    no_more_prio = true;
                }
            }

            members.extend(rem);
            members
        }
    }
}

impl<'a, T> Bucket<'a, T>
where T: Eq + Hash + Copy
{
    fn new(key: T, mem: MemberStatsHandle<'a>) -> Bucket<'a, T> {
        Bucket {
            key,
            mem: vec![mem],
        }
    }

    fn add(&mut self, mem: MemberStatsHandle<'a>) {
        self.mem.push(mem);
    }
}

fn filter_into_buckets<'a, T, F>(mem: Vec<MemberStatsHandle<'a>>, filter: F)
    -> Vec<Bucket<'a, T>>
where
    T: Eq + Hash + Copy,
    F: Fn(&MemberStatsHandle) -> T,
{
    let mut map: HashMap<T, Bucket<T>> = HashMap::new();

    mem.into_iter().for_each(|handle| {
        let key = filter(&handle);
        match map.get_mut(&key) {
            Some(bucket) => { bucket.add(handle); },
            None => { map.insert(key, Bucket::new(key, handle)); }
        }
    });

    map.into_values().collect()
}

fn sort_inside_buckets<'a, T>(
    buckets: Vec<Bucket<'a, T>>,
    rules: &Vec<OrderRule>,
    context: &Context,
    index: usize
)
    -> Vec<Bucket<'a, T>>
where
    T: Eq + Hash + Copy
{
    buckets.into_iter().map(|b| do_order_bucket(b, rules, context, index)).collect()
}

fn do_order_bucket<'a, T>(
    bucket: Bucket<'a, T>,
    rules: &Vec<OrderRule>,
    context: &Context,
    index: usize
) -> Bucket<'a, T>
where
    T: Eq + Hash + Copy
{
    let Bucket { key, mem } = bucket;
    let mem = do_order(mem, rules, context, index);
    Bucket { key, mem }
}

fn sort_and_reduce_buckets<'a, T>(mut buckets: Vec<Bucket<'a, T>>)
    -> Vec<MemberStatsHandle<'a>>
where
    T: Eq + Hash + Copy + Ord
{
    buckets.sort_by(|a, b| a.key.cmp(&b.key));
    buckets.into_iter().map(|b| b.mem.into_iter()).flatten().collect()
}


#[cfg(test)]
mod tests {
    use crate::ordering::{Context, OrderRule, order};
    use crate::session::{Member, MemberStatsHandle};
    use crate::profile::Profile;

    fn member(order: u64, speeches: u32) -> Member {
        let mut mem = Member::new("Test".to_owned(), false, 0);
        mem.ask_to_speak(order);
        mem.incr_speeches_by(speeches);
        mem
    }

    fn member_prio(order: u64, speeches: u32) -> Member {
        let mut mem = Member::new("Test".to_owned(), true, 0);
        mem.ask_to_speak(order);
        mem.incr_speeches_by(speeches);
        mem
    }

    #[test]
    fn sort_by_speeches_then_ask_order() {
        let mems = vec![
            member(1, 2),
            member(3, 2),
            member(2, 0),
            member(4, 0),
        ];

        let mut profile = Profile::new(0);
        profile.order_rules = vec![
            OrderRule::SpeechCount,
            OrderRule::FirstAsked,
        ];

        let mems_ref: Vec<MemberStatsHandle> = mems
            .iter()
            .enumerate()
            .map(|(i, m)| m.stats_handle(i, &profile))
            .collect();

        let context = Context { last_speaker_nonprio: false };
        let mems_ref = order(mems_ref, &profile.order_rules, &context);

        assert_eq!(
            mems_ref.iter().map(|m| m.id()).collect::<Vec<usize>>(),
            vec![
                2,
                3,
                0,
                1,
            ]
        );
    }

    #[test]
    fn prio_alternate_then_ask_order() {
        let mems = vec![
            member(0, 0),
            member(1, 0),
            member(2, 0),
            member(3, 0),
            member_prio(4, 1),
            member_prio(5, 2),
        ];

        let mut profile = Profile::new(0);
        profile.order_rules = vec![
            OrderRule::PrioAlternate,
            OrderRule::FirstAsked,
        ];

        let mems_ref: Vec<MemberStatsHandle> = mems
            .iter()
            .enumerate()
            .map(|(i, m)| m.stats_handle(i, &profile))
            .collect();

        let context = Context { last_speaker_nonprio: false };
        let mems_ref = order(mems_ref, &profile.order_rules, &context);

        assert_eq!(
            mems_ref.iter().map(|m| m.id()).collect::<Vec<usize>>(),
            vec![
                0,
                4,
                1,
                5,
                2,
                3,
            ]
        );
    }
}
