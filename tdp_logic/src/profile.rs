mod storage;

pub use self::storage::StoredProfile;

use serde::{Deserialize, Serialize};

use crate::ordering::OrderRule;

#[derive(Serialize, Deserialize, Clone)]
pub struct Profile {
    pub id: u64,
    pub name: String,
    pub timelimit_first: u64,
    pub timelimit_next: u64,
    pub order_rules: Vec<OrderRule>,

    pub reset_timelimit: bool,
    pub reset_speeches: bool,
    pub reset_timer: bool,
    pub reset_stats: bool,
}

impl Profile {
    pub fn new(id: u64) -> Self {
        Profile {
            id,
            name: String::from("Défaut"),
            timelimit_first: 180000,
            timelimit_next: 150000,
            order_rules: vec![
                OrderRule::PrioAlternate,
                OrderRule::SpeechCount,
                OrderRule::FirstAsked
            ],

            reset_timelimit: true,
            reset_speeches: true,
            reset_timer: true,
            reset_stats: false,
        }
    }

    pub fn add_rule(&mut self, rule: OrderRule) {
        if self.order_rules.contains(&rule) {
            return;
        }

        if rule.must_be_last() {
            if self.order_rules.last().is_some_and(|r| r.must_be_last()) {
                self.order_rules.pop();
            }
            self.order_rules.push(rule);
        } else {
            if self.order_rules.last().is_some_and(|r| r.must_be_last()) {
                self.order_rules.insert(self.order_rules.len() - 1, rule);
            } else {
                self.order_rules.push(rule);
            }
        }
    }

    pub fn remove_rule(&mut self, rule: OrderRule) {
        if !rule.must_be_last() {
            self.order_rules.retain(|r| *r != rule);
        }
    }

    pub fn clear_rules(&mut self) {
        self.order_rules.retain(|r| r.must_be_last());
    }

    pub fn move_rule(&mut self, rule: OrderRule, delta: isize) {
        let old_idx = self.order_rules.iter().position(|o| *o == rule);
        if old_idx.is_none() {
            return;
        }
        let old_idx = old_idx.unwrap();

        self.order_rules.remove(old_idx);

        let new_idx: isize = isize::try_from(old_idx).unwrap() + delta;
        let max_index = self.order_rules.len()
            - if self.order_rules.iter().last().is_some_and(|r| r.must_be_last()) { 1 } else { 0 };

        let mut new_idx: usize = new_idx.try_into().unwrap_or(0);

        if new_idx > max_index {
            new_idx = max_index;
        }

        self.order_rules.insert(new_idx.into(), rule);
    }
}
