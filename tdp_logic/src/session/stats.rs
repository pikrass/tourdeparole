use std::collections::{HashMap, hash_map};

use serde::{Serialize, Deserialize};

use crate::profile::Profile;

use super::{Session, Member};

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SessionStats {
    pub totals: SubsessionStats,
    pub subsessions: Vec<SubsessionStats>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct SubsessionStats {
    pub prio: Option<StatsPrio>,
    pub members: Vec<MemberStats>,
}

#[derive(Default, Serialize, Deserialize, Clone, PartialEq)]
pub struct StatsPrio {
    pub members: (usize, usize),
    pub speeches: (u32, u32),
    pub time: (u64, u64),
}

#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub struct MemberStats {
    pub name: String,
    pub prio: bool,
    pub speeches: u32,
    pub time: u64,
}

impl Session {
    pub fn stats(&self) -> SessionStats {
        let mut total_prio: StatsPrio = Default::default();
        let mut total_members: HashMap<usize, MemberStats> = HashMap::new();
        let mut subsessions: Vec<SubsessionStats> = Vec::new();

        for i in 0..=self.subsession {
            let mut p_stats: StatsPrio = Default::default();
            let mut m_stats = Vec::new();

            for (id, member) in self.members.iter().enumerate() {
                if !member.was_active(i) {
                    continue;
                }

                let stats = MemberStats {
                    name: member.name().to_owned(),
                    prio: member.prio(),
                    speeches: member.speeches_for(i),
                    time: member.timer_for(i),
                };

                p_stats.add(member, i);

                match total_members.entry(id) {
                    hash_map::Entry::Occupied(mut entry) => {
                        let t_stats = entry.get_mut();
                        t_stats.speeches += stats.speeches;
                        t_stats.time += stats.time;
                        total_prio.add_stats_only(member, i);
                    }
                    hash_map::Entry::Vacant(entry) => {
                        entry.insert(stats.clone());
                        total_prio.add(member, i);
                    }
                }

                m_stats.push(stats);
            }

            subsessions.push(SubsessionStats {
                prio: if p_stats.should_show() { Some(p_stats) } else { None },
                members: m_stats
            });
        }

        SessionStats {
            totals: SubsessionStats {
                prio: if total_prio.should_show() { Some(total_prio) } else { None },
                members: total_members.into_values().collect(),
            },
            subsessions,
        }
    }

    pub fn stats_prio(&self, profile: &Profile) -> Option<StatsPrio> {
        let (mem_prio, mem_nonprio): (Vec<&Member>, Vec<&Member>) = self.members
                                      .iter()
                                      .partition(|m| m.prio());

        let map_stat = if profile.reset_stats {
            |m: &Member| (1usize, m.last_speeches(), m.last_timer())
        } else {
            |m: &Member| (1usize, m.all_speeches(), m.all_timer())
        };

        let into_stats = |members: Vec<&Member>| members.into_iter()
            .filter(|m| !m.hidden())
            .map(map_stat)
            .fold((0usize, 0u32, 0u64), |acc, e| (acc.0 + e.0, acc.1 + e.1, acc.2 + e.2));

        let stats_nonprio = into_stats(mem_nonprio);
        let stats_prio = into_stats(mem_prio);

        let stats_struct = StatsPrio {
            members: (stats_nonprio.0, stats_prio.0),
            speeches: (stats_nonprio.1, stats_prio.1),
            time: (stats_nonprio.2, stats_prio.2),
        };

        if stats_struct.should_show() {
            Some(stats_struct)
        } else {
            None
        }
    }
}

impl StatsPrio {
    pub fn add(&mut self, member: &Member, subsession: usize) {
        if member.prio() {
            self.members.1 += 1;
        } else {
            self.members.0 += 1;
        }
        self.add_stats_only(member, subsession);
    }

    pub fn add_stats_only(&mut self, member: &Member, subsession: usize) {
        if member.prio() {
            self.speeches.1 += member.speeches_for(subsession);
            self.time.1 += member.timer_for(subsession);
        } else {
            self.speeches.0 += member.speeches_for(subsession);
            self.time.0 += member.timer_for(subsession);
        }
    }

    pub fn should_show(&self) -> bool {
        self.members.0 > 0
            && self.members.1 > 0
            && self.speeches.0 + self.speeches.1 > 0
            && self.time.0 + self.time.1 > 0
    }
}
