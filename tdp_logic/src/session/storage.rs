use serde::{Serialize, Deserialize};

use super::{Session, DateTimeWithOffset, MemberSubsession, Member, SessionIndexEntry};

#[derive(Serialize, Deserialize)]
#[allow(private_interfaces)]
pub enum StoredSession {
    V1(StoredSessionV1),
}

#[derive(Serialize, Deserialize)]
#[allow(private_interfaces)]
pub enum StoredSessionIndexEntry {
    V1(StoredSessionIndexEntryV1),
}

#[derive(Serialize, Deserialize)]
struct StoredSessionV1 {
    id: u64,
    profile_id: u64,
    started_at: Option<StoredDateTimeWithOffsetV1>,
    updated_at: Option<StoredDateTimeWithOffsetV1>,
    subsession: usize,

    members: Vec<StoredMemberV1>,
    active_speaker: Option<usize>,
    last_speaker_nonprio: bool,
    first_speech_in_subsession: bool,

    timer: u64,
}

#[derive(Serialize, Deserialize)]
struct StoredSessionIndexEntryV1 {
    id: u64,
    profile_id: u64,
    name: String,
    started_at: Option<StoredDateTimeWithOffsetV1>,
    updated_at: Option<StoredDateTimeWithOffsetV1>,
}

#[derive(Serialize, Deserialize)]
struct StoredDateTimeWithOffsetV1 {
    datetime: i64, // in milliseconds
    offset: i32,   // in minutes
}

#[derive(Serialize, Deserialize)]
struct StoredMemberV1 {
    name: String,
    prio: bool,
    hidden: bool,
    asking_to_speak: Option<u64>,
    subsessions: Vec<StoredMemberSubsessionV1>,
}

#[derive(Serialize, Deserialize)]
struct StoredMemberSubsessionV1 {
    was_active: bool,
    speeches: u32,
    timer: u64,
}

// ====== Storing ======

impl From<&Session> for StoredSession {
    fn from(session: &Session) -> Self {
        Self::V1(StoredSessionV1 {
            id: session.id,
            profile_id: session.profile_id,
            started_at: session.started_at.as_ref().map(|dto| dto.into()),
            updated_at: session.updated_at.as_ref().map(|dto| dto.into()),
            subsession: session.subsession,

            members: session.members.iter().map(|s| s.into()).collect(),
            active_speaker: session.active_speaker,
            last_speaker_nonprio: session.last_speaker_nonprio,
            first_speech_in_subsession: session.first_speech_in_subsession,

            timer: session.timer,
        })
    }
}

impl From<&SessionIndexEntry> for StoredSessionIndexEntry {
    fn from(entry: &SessionIndexEntry) -> Self {
        Self::V1(StoredSessionIndexEntryV1 {
            id: entry.id,
            profile_id: entry.profile_id,
            name: entry.name.clone(),
            started_at: entry.started_at.as_ref().map(|dto| dto.into()),
            updated_at: entry.updated_at.as_ref().map(|dto| dto.into()),
        })
    }
}

impl From<&DateTimeWithOffset> for StoredDateTimeWithOffsetV1 {
    fn from(value: &DateTimeWithOffset) -> Self {
        StoredDateTimeWithOffsetV1 {
            datetime: value.datetime,
            offset: value.offset,
        }
    }
}

impl From<&Member> for StoredMemberV1 {
    fn from(mem: &Member) -> Self {
        StoredMemberV1 {
            name: mem.name.clone(),
            prio: mem.prio,
            hidden: mem.hidden,
            asking_to_speak: mem.asking_to_speak,
            subsessions: mem.subsessions.iter().map(|s| s.into()).collect(),
        }
    }
}

impl From<&MemberSubsession> for StoredMemberSubsessionV1 {
    fn from(sub: &MemberSubsession) -> Self {
        StoredMemberSubsessionV1 {
            was_active: sub.was_active,
            speeches: sub.speeches,
            timer: sub.timer,
        }
    }
}

// ====== Restoring ======

impl From<StoredSession> for Session {
    fn from(storage: StoredSession) -> Self {
        match storage {
            StoredSession::V1(session) => session.into(),
        }
    }
}

impl From<StoredSessionIndexEntry> for SessionIndexEntry {
    fn from(storage: StoredSessionIndexEntry) -> Self {
        match storage {
            StoredSessionIndexEntry::V1(entry) => entry.into(),
        }
    }
}

impl From<StoredSessionV1> for Session {
    fn from(session: StoredSessionV1) -> Self {
        Session {
            id: session.id,
            profile_id: session.profile_id,
            started_at: session.started_at.map(|dto| dto.into()),
            updated_at: session.updated_at.map(|dto| dto.into()),
            subsession: session.subsession,

            members: session.members.into_iter().map(|m| m.into()).collect(),
            active_speaker: session.active_speaker,
            last_speaker_nonprio: session.last_speaker_nonprio,
            first_speech_in_subsession: session.first_speech_in_subsession,

            timer: session.timer,
            timer_last_tick: None,
        }
    }
}

impl From<StoredSessionIndexEntryV1> for SessionIndexEntry {
    fn from(entry: StoredSessionIndexEntryV1) -> Self {
        SessionIndexEntry {
            id: entry.id,
            profile_id: entry.profile_id,
            name: entry.name,
            started_at: entry.started_at.map(|dto| dto.into()),
            updated_at: entry.updated_at.map(|dto| dto.into()),
        }
    }
}

impl From<StoredDateTimeWithOffsetV1> for DateTimeWithOffset {
    fn from(value: StoredDateTimeWithOffsetV1) -> Self {
        DateTimeWithOffset {
            datetime: value.datetime,
            offset: value.offset,
        }
    }
}

impl From<StoredMemberV1> for Member {
    fn from(mem: StoredMemberV1) -> Self {
        Member {
            name: mem.name,
            prio: mem.prio,
            hidden: mem.hidden,
            asking_to_speak: mem.asking_to_speak,
            subsessions: mem.subsessions.into_iter().map(|s| s.into()).collect(),
        }
    }
}

impl From<StoredMemberSubsessionV1> for MemberSubsession {
    fn from(sub: StoredMemberSubsessionV1) -> Self {
        MemberSubsession {
            was_active: sub.was_active,
            speeches: sub.speeches,
            timer: sub.timer,
        }
    }
}
