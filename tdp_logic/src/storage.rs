use std::collections::VecDeque;

use ron::error::SpannedResult;

use crate::profile::{Profile, StoredProfile};
use crate::session::{Session, StoredSession, SessionIndexEntry, StoredSessionIndexEntry};

// ====== Storing ======

pub fn ser_profiles(profiles: &Vec<Profile>) -> String {
    let stored_profiles: Vec<StoredProfile> =
        profiles.iter().map(|p| StoredProfile::from(p)).collect();
    ron::to_string(&stored_profiles).unwrap()
}

pub fn ser_session_index(entries: &VecDeque<SessionIndexEntry>) -> String {
    let stored_entries: Vec<StoredSessionIndexEntry> =
        entries.iter().map(|p| StoredSessionIndexEntry::from(p)).collect();
    ron::to_string(&stored_entries).unwrap()
}

pub fn ser_session(session: &Session) -> String {
    ron::to_string(&StoredSession::from(session)).unwrap()
}

// ====== Restoring ======

pub fn deser_profiles(ron_str: &str) -> SpannedResult<Vec<Profile>> {
    let versioned = ron::from_str::<Vec<StoredProfile>>(ron_str);
    versioned
        .map(|vec|
            vec.into_iter()
            .map(|p| p.into())
            .collect()
        )
}

pub fn deser_session_index(ron_str: &str) -> SpannedResult<VecDeque<SessionIndexEntry>> {
    let versioned = ron::from_str::<VecDeque<StoredSessionIndexEntry>>(ron_str);
    versioned
        .map(|vec|
            vec.into_iter()
            .map(|p| p.into())
            .collect()
        )
}

pub fn deser_session(ron_str: &str) -> SpannedResult<Session> {
    let versioned = ron::from_str::<StoredSession>(ron_str);
    versioned.map(|sess| sess.into())
}
