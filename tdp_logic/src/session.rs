mod stats;
mod storage;

pub use self::stats::SessionStats;
pub use self::storage::{StoredSession, StoredSessionIndexEntry};

use serde::{Deserialize, Serialize};

use crate::profile::Profile;
use crate::ordering::{Context, order};

use self::stats::StatsPrio;

#[derive(Serialize, Deserialize, Clone)]
pub struct Session {
    id: u64,
    profile_id: u64,
    started_at: Option<DateTimeWithOffset>,
    updated_at: Option<DateTimeWithOffset>,
    subsession: usize,

    members: Vec<Member>,
    active_speaker: Option<usize>,
    last_speaker_nonprio: bool,
    first_speech_in_subsession: bool,

    timer: u64,
    timer_last_tick: Option<u64>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SessionView {
    pub id: u64,
    pub profile_id: u64,
    pub members: Vec<MemberView>,
    pub active_speaker: Option<String>,
    pub queue: Vec<String>,

    pub timer: u64,
    pub timer_active: bool,
    pub time_limit: Option<u64>,

    pub stats_prio: Option<StatsPrio>,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct SessionIndexEntry {
    pub id: u64,
    pub profile_id: u64,
    pub name: String,
    pub started_at: Option<DateTimeWithOffset>,
    pub updated_at: Option<DateTimeWithOffset>,
}

#[derive(Serialize, Deserialize, Clone, Copy)]
pub struct DateTimeWithOffset {
    pub datetime: i64, // in milliseconds
    pub offset: i32,   // in minutes
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct Member {
    name: String,
    prio: bool,
    hidden: bool,
    asking_to_speak: Option<u64>,
    subsessions: Vec<MemberSubsession>,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
struct MemberSubsession {
    pub was_active: bool,
    pub speeches: u32,
    pub timer: u64,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct MemberView {
    pub id: usize,
    pub name: String,
    pub prio: bool,
    pub hidden: bool,
    pub asking_to_speak: bool,
    pub speeches: u32,
    pub can_delete: bool,
}

pub struct MemberStatsHandle<'a> {
    member: &'a Member,
    id: usize,
    reset_speeches: bool,
    reset_timer: bool,
}

impl Session {
    pub fn new(id: u64, profile_id: u64) -> Self {
        Self {
            id,
            profile_id,
            started_at: None,
            updated_at: None,
            subsession: 0,
            members: Vec::new(),
            active_speaker: None,
            last_speaker_nonprio: false,
            first_speech_in_subsession: true,
            timer: 0,
            timer_last_tick: None,
        }
    }

    pub fn id(&self) -> u64 {
        self.id
    }

    pub fn profile(&self) -> u64 {
        self.profile_id
    }

    pub fn set_profile(&mut self, profile_id: u64) {
        self.profile_id = profile_id;
    }

    pub fn create_member(&mut self, name: String, prio: bool) {
        self.members.push(Member::new(name, prio, self.subsession));
    }

    pub fn can_delete_member(&self, id: usize) -> bool {
        self.members.get(id).is_some_and(|mem| {
            mem.all_speeches() == 0 && mem.all_timer() == 0
        })
    }

    pub fn delete_member(&mut self, id: usize) {
        if self.can_delete_member(id) {
            self.members.remove(id);
        }
    }

    pub fn toggle_member_prio(&mut self, id: usize) {
        if let Some(mem) = self.members.get_mut(id) {
            mem.prio = !mem.prio;
        }
    }

    pub fn toggle_member_hidden(&mut self, id: usize) {
        if let Some(mem) = self.members.get_mut(id) {
            mem.set_hidden(!mem.hidden());
        }
    }

    pub fn wants_to_speak(&self, id: usize) -> Option<bool> {
        self.members.get(id).map(|m| m.wants_to_speak())
    }

    pub fn ask_to_speak(&mut self, id: usize, time: u64) {
        if let Some(mem) = self.members.get_mut(id) {
            mem.ask_to_speak(time);
        }
    }

    pub fn cancel_ask_to_speak(&mut self, id: usize) {
        if let Some(mem) = self.members.get_mut(id) {
            mem.cancel_ask_to_speak();
        }

        if self.active_speaker == Some(id) {
            self.reset_timer();
            self.active_speaker = None;
        }
    }

    pub fn active_speaker(&self) -> Option<&Member> {
        self.active_speaker.and_then(|id| self.members.get(id))
    }

    pub fn start_timer(&mut self, time: u64) {
        self.timer_last_tick = Some(time);
    }

    pub fn stop_timer(&mut self) {
        self.timer_last_tick = None;
    }

    pub fn reset_timer(&mut self) {
        self.stop_timer();
        self.timer = 0;
    }

    pub fn tick(&mut self, time: u64) {
        if let Some(last_tick) = self.timer_last_tick.take() {
            if time > last_tick {
                self.timer += time - last_tick;
                if let Some(ref mut mem) = self.active_speaker.and_then(|i| self.members.get_mut(i)) {
                    mem.incr_timer(time - last_tick);
                }
            }
            self.timer_last_tick = Some(time);
        }
    }

    pub fn sort_by_name(&self) -> Vec<(usize, &Member)> {
        let mut refs_vec: Vec<(usize, &Member)> = self.members.iter().enumerate().collect();
        refs_vec.sort_by(|(_i, a), (_j, b)| a.name.cmp(&b.name));
        refs_vec
    }

    pub fn queue(&self, profile: &Profile) -> Vec<MemberStatsHandle> {
        let refs_vec: Vec<MemberStatsHandle> = self.members
            .iter()
            .enumerate()
            .filter(|(i, m)| m.asking_to_speak.is_some()
                    && !self.active_speaker.is_some_and(|j| *i == j))
            .map(|(i, m)| m.stats_handle(i, profile))
            .collect();

        let context = Context {
            last_speaker_nonprio: if profile.reset_speeches {
                    self.last_speaker_nonprio && !self.first_speech_in_subsession
                } else {
                    self.last_speaker_nonprio
                }
        };
        order(refs_vec, &profile.order_rules, &context)
    }

    pub fn next(&mut self, profile: &Profile) {
        self.end_turn();

        if let Some(id) = self.queue(profile).get(0).map(|m| m.id()) {
            self.active_speaker = Some(id);
        }
    }

    pub fn force_speaker(&mut self, id: usize, time: u64) {
        if !self.active_speaker.is_some_and(|i| i == id) {
            self.end_turn();

            if let Some(mem) = self.members.get_mut(id) {
                if mem.asking_to_speak.is_none() {
                    mem.asking_to_speak = Some(time);
                }

                self.active_speaker = Some(id);
            }
        }
    }

    fn end_turn(&mut self) {
        self.reset_timer();

        if let Some(speaker) = self.active_speaker.take().and_then(|i| self.members.get_mut(i)) {
            speaker.spoke();
            self.last_speaker_nonprio = !speaker.prio();
            self.first_speech_in_subsession = false;
        }
    }

    pub fn next_subsession(&mut self) {
        self.end_turn();

        self.subsession += 1;
        self.first_speech_in_subsession = true;

        for mem in self.members.iter_mut() {
            mem.next_subsession();
        }
    }

    pub fn update_times(&mut self, datetime: i64, offset: i32) {
        let now = DateTimeWithOffset { datetime, offset };
        self.started_at.get_or_insert(now);
        self.updated_at = Some(now);
    }

    pub fn view(&self, profile: &Profile) -> SessionView {
        SessionView {
            id: self.id,
            profile_id: self.profile_id,
            timer: self.timer,
            timer_active: self.timer_last_tick.is_some(),
            time_limit: self.active_speaker
                .and_then(|i| self.members.get(i))
                .map(|m| {
                    if profile.reset_timelimit && m.last_speeches() == 0 {
                        profile.timelimit_first
                    } else if !profile.reset_timelimit && m.all_speeches() == 0 {
                        profile.timelimit_first
                    } else {
                        profile.timelimit_next
                    }
                }),

            members: self.sort_by_name().into_iter().map( |(i, m)| {
                    let speeches = if profile.reset_speeches {
                        m.last_speeches()
                    } else {
                        m.all_speeches()
                    };

                    MemberView {
                        id: i,
                        name: m.name().to_owned(),
                        prio: m.prio(),
                        hidden: m.hidden(),
                        asking_to_speak: m.wants_to_speak(),
                        speeches,
                        can_delete: self.can_delete_member(i),
                    }
            }).collect(),

            stats_prio: self.stats_prio(profile),

            active_speaker: self.active_speaker
                .and_then(|i| self.members.get(i))
                .and_then(|m| Some(m.name.clone())),

            queue: self.queue(profile).into_iter()
                .map(|m| m.name().to_owned())
                .collect(),
        }
    }

    pub fn index_entry(&self) -> SessionIndexEntry {
        SessionIndexEntry {
            id: self.id,
            profile_id: self.profile_id,
            name: String::from("Session"),
            started_at: self.started_at,
            updated_at: self.updated_at,
        }
    }
}

impl Member {
    pub fn new(name: String, prio: bool, cur_subsession: usize) -> Member {
        let mut subsessions = Vec::new();

        // Previous subsessions
        for _ in 0..cur_subsession {
            subsessions.push(MemberSubsession {
                was_active: false,
                speeches: 0,
                timer: 0,
            });
        }

        // Current subsession
        subsessions.push(MemberSubsession {
            was_active: true,
            speeches: 0,
            timer: 0,
        });

        Member {
            name,
            prio,
            hidden: false,
            asking_to_speak: None,
            subsessions,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn prio(&self) -> bool {
        self.prio
    }

    pub fn hidden(&self) -> bool {
        self.hidden
    }

    pub fn set_hidden(&mut self, hidden: bool) {
        self.hidden = hidden;
        if let Some(sub) = self.subsessions.last_mut() {
            sub.was_active = !hidden || sub.speeches > 0 || sub.timer > 0;
        }
    }

    pub fn next_subsession(&mut self) {
        self.subsessions.push(MemberSubsession {
            was_active: !self.hidden,
            speeches: 0,
            timer: 0,
        });
    }

    pub fn wants_to_speak(&self) -> bool {
        self.asking_to_speak.is_some()
    }

    pub fn time_asked_to_speak(&self) -> Option<u64> {
        self.asking_to_speak.clone()
    }

    pub fn ask_to_speak(&mut self, time: u64) {
        self.asking_to_speak = Some(time);
    }

    pub fn cancel_ask_to_speak(&mut self) {
        self.asking_to_speak = None;
    }

    pub fn all_speeches(&self) -> u32 {
        self.subsessions.iter()
            .map(|s| s.speeches)
            .sum()
    }

    pub fn last_speeches(&self) -> u32 {
        self.subsessions.last().map(|s| s.speeches).unwrap_or(0)
    }

    pub fn speeches_for(&self, subsession: usize) -> u32 {
        self.subsessions.get(subsession).map(|s| s.speeches).unwrap_or(0)
    }

    pub fn spoke(&mut self) {
        self.cancel_ask_to_speak();
        self.incr_speeches_by(1);
    }

    pub fn incr_speeches_by(&mut self, nb: u32) {
        if let Some(s) = self.subsessions.last_mut() {
            s.speeches += nb;
        }
    }

    pub fn all_timer(&self) -> u64 {
        self.subsessions.iter()
            .map(|s| s.timer)
            .sum()
    }

    pub fn last_timer(&self) -> u64 {
        self.subsessions.last().map(|s| s.timer).unwrap_or(0)
    }

    pub fn timer_for(&self, subsession: usize) -> u64 {
        self.subsessions.get(subsession).map(|s| s.timer).unwrap_or(0)
    }

    pub fn incr_timer(&mut self, time: u64) {
        if let Some(s) = self.subsessions.last_mut() {
            s.timer += time;
        }
    }

    pub fn was_active(&self, subsession: usize) -> bool {
        self.subsessions.get(subsession).map(|s| s.was_active).unwrap_or(false)
    }

    pub fn stats_handle<'a, 'b>(
        &'a self,
        id: usize,
        profile: &'b Profile,
    ) -> MemberStatsHandle<'a> {
        MemberStatsHandle::new(self, id, profile)
    }
}

impl<'a> MemberStatsHandle<'a> {
    fn new(member: &'a Member, id: usize, profile: &Profile) -> Self {
        MemberStatsHandle {
            member: &member,
            id,
            reset_speeches: profile.reset_speeches,
            reset_timer: profile.reset_timer,
        }
    }

    pub fn id(&self) -> usize {
        self.id
    }

    pub fn name(&self) -> &str {
        self.member.name()
    }

    pub fn prio(&self) -> bool {
        self.member.prio()
    }

    pub fn time_asked_to_speak(&self) -> Option<u64> {
        self.member.time_asked_to_speak()
    }

    pub fn speeches(&self) -> u32 {
        if self.reset_speeches {
            self.member.last_speeches()
        } else {
            self.member.all_speeches()
        }
    }

    pub fn timer(&self) -> u64 {
        if self.reset_timer {
            self.member.last_timer()
        } else {
            self.member.all_timer()
        }
    }
}
