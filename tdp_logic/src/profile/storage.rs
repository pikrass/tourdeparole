use serde::{Serialize, Deserialize};

use crate::ordering::OrderRule;

use super::Profile;

#[derive(Serialize, Deserialize)]
#[allow(private_interfaces)]
pub enum StoredProfile {
    V1(StoredProfileV1),
}

#[derive(Serialize, Deserialize)]
struct StoredProfileV1 {
    id: u64,
    name: String,
    timelimit_first: u64,
    timelimit_next: u64,
    order_rules: Vec<StoredOrderRuleV1>,

    reset_timelimit: bool,
    reset_speeches: bool,
    reset_timer: bool,
    reset_stats: bool,
}

#[derive(Serialize, Deserialize)]
enum StoredOrderRuleV1 {
    FirstAsked,
    SpeechCount,
    TimeUsed,
    PrioFirst,
    PrioAlternate,
}

// ====== Storing ======

impl From<&Profile> for StoredProfile {
    fn from(profile: &Profile) -> Self {
        StoredProfile::V1(
            StoredProfileV1 {
                id: profile.id,
                name: profile.name.clone(),
                timelimit_first: profile.timelimit_first,
                timelimit_next: profile.timelimit_next,
                order_rules: profile.order_rules.iter().map(|o| o.into()).collect(),
                reset_timelimit: profile.reset_timelimit,
                reset_speeches: profile.reset_speeches,
                reset_timer: profile.reset_timer,
                reset_stats: profile.reset_stats,
            }
        )
    }
}

impl From<&OrderRule> for StoredOrderRuleV1 {
    fn from(rule: &OrderRule) -> Self {
        match rule {
            OrderRule::FirstAsked => StoredOrderRuleV1::FirstAsked,
            OrderRule::SpeechCount => StoredOrderRuleV1::SpeechCount,
            OrderRule::TimeUsed => StoredOrderRuleV1::TimeUsed,
            OrderRule::PrioFirst => StoredOrderRuleV1::PrioFirst,
            OrderRule::PrioAlternate => StoredOrderRuleV1::PrioAlternate,
        }
    }
}

// ====== Restoring ======

impl From<StoredProfile> for Profile {
    fn from(storage: StoredProfile) -> Self {
        match storage {
            StoredProfile::V1(profile) => profile.into(),
        }
    }
}

impl From<StoredProfileV1> for Profile {
    fn from(profile: StoredProfileV1) -> Self {
        Profile {
            id: profile.id,
            name: profile.name,
            timelimit_first: profile.timelimit_first,
            timelimit_next: profile.timelimit_next,
            order_rules: profile.order_rules.into_iter().map(|o| o.into()).collect(),
            reset_timelimit: profile.reset_timelimit,
            reset_speeches: profile.reset_speeches,
            reset_timer: profile.reset_timer,
            reset_stats: profile.reset_stats,
        }
    }
}

impl From<StoredOrderRuleV1> for OrderRule {
    fn from(rule: StoredOrderRuleV1) -> Self {
        match rule {
            StoredOrderRuleV1::FirstAsked => OrderRule::FirstAsked,
            StoredOrderRuleV1::SpeechCount => OrderRule::SpeechCount,
            StoredOrderRuleV1::TimeUsed => OrderRule::TimeUsed,
            StoredOrderRuleV1::PrioFirst => OrderRule::PrioFirst,
            StoredOrderRuleV1::PrioAlternate => OrderRule::PrioAlternate,
        }
    }
}
