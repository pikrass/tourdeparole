use crux_core::typegen::TypeGen;
use tdp_logic::Tdp;
use tdp_logic::nav::{Screen, ScreenCat, ProfileScreenView, SessionScreenView, HistoryScreenView};
use tdp_logic::ordering::OrderRule;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=../tdp_logic");

    let mut gen = TypeGen::new();

    gen.register_app::<Tdp>().expect("register");
    gen.register_type::<OrderRule>().expect("register");
    gen.register_type::<Screen>().expect("register");
    gen.register_type::<ScreenCat>().expect("register");
    gen.register_type::<ProfileScreenView>().expect("register");
    gen.register_type::<SessionScreenView>().expect("register");
    gen.register_type::<HistoryScreenView>().expect("register");

    let output_root = PathBuf::from("./generated");

    gen.java("net.pikrass.tourdeparole.tdp_types", output_root.join("java"))
        .expect("java type gen failed");
}
