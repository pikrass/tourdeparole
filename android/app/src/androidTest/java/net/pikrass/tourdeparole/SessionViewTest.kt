package net.pikrass.tourdeparole

import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.MemberView
import net.pikrass.tourdeparole.ui.SessionView
import org.junit.Rule
import org.junit.Test
import java.util.Optional

class SessionViewTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun idsUpdateAfterMemberCreated() {
        val members = mutableStateListOf(
            net.pikrass.tourdeparole.tdp_types.MemberView(
                /* id */ 1,
                /* name */ "Eva",
                /* prio */ false,
                /* hidden */ false,
                /* asking_to_speak */ false,
                /* speeches */ 0,
                /* can_delete */ true,
            )
        )

        val session = net.pikrass.tourdeparole.tdp_types.SessionView(
            /* id */ 1,
            /* profile_id */ 1,
            /* members */ members,
            /* active_speaker */ Optional.empty(),
            /* queue */ listOf<String>(),
            /* timer */ 0,
            /* timer_active */ false,
            /* time_limit */ Optional.empty(),
            /* stats_prio */ Optional.empty(),
        )

        var updateCount = 0
        var lastUpdate: Event? = null

        val update = { evt: Event ->
            updateCount++
            lastUpdate = evt
        }

        composeTestRule.setContent {
            SessionView(data = session, update = update)
        }

        members.add(0, net.pikrass.tourdeparole.tdp_types.MemberView(
                /* id */ 2,
                /* name */ "Alice",
                /* prio */ false,
                /* hidden */ false,
                /* asking_to_speak */ false,
                /* speeches */ 0,
                /* can_delete */ true,
        ))

        composeTestRule.onNodeWithText("Eva").assertExists()
        composeTestRule.onNodeWithText("Alice").assertExists()

        composeTestRule.onNodeWithText("Eva").performClick()

        assert(updateCount == 1)
        assert(lastUpdate != null)
        assert(lastUpdate is Event.ToggleAskToSpeak)
        assert((lastUpdate as Event.ToggleAskToSpeak).value == 1L)

        composeTestRule.onNodeWithText("Alice").performClick()

        assert(updateCount == 2)
        assert(lastUpdate != null)
        assert(lastUpdate is Event.ToggleAskToSpeak)
        assert((lastUpdate as Event.ToggleAskToSpeak).value == 2L)
    }
}