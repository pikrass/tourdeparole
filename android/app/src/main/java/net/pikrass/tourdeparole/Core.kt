package net.pikrass.tourdeparole

import android.app.Application
import android.content.Context
import android.os.SystemClock
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.pikrass.tourdeparole.tdp_logic.handleResponse

import net.pikrass.tourdeparole.tdp_logic.processEvent
import net.pikrass.tourdeparole.tdp_logic.view
import net.pikrass.tourdeparole.tdp_types.DatetimeOperation
import net.pikrass.tourdeparole.tdp_types.DatetimeOutput
import net.pikrass.tourdeparole.tdp_types.Effect
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.RandomOutput
import net.pikrass.tourdeparole.tdp_types.Request
import net.pikrass.tourdeparole.tdp_types.Requests
import net.pikrass.tourdeparole.tdp_types.StorageOperation
import net.pikrass.tourdeparole.tdp_types.StorageOutput
import net.pikrass.tourdeparole.tdp_types.ViewModel
import java.io.FileNotFoundException
import java.util.Optional
import java.util.Random
import java.util.TimeZone
import java.util.concurrent.atomic.AtomicBoolean

class Core(application: Application) : androidx.lifecycle.AndroidViewModel(application) {
    var view: ViewModel by mutableStateOf(ViewModel.bincodeDeserialize(view()))
        private set

    var rnd = Random()

    var ticking = AtomicBoolean()

    suspend fun update(event: Event) {
        val effects = processEvent(event.bincodeSerialize())

        val requests = Requests.bincodeDeserialize(effects)
        for (request in requests) {
            processEffect(request)
        }
    }

    private suspend fun processEffect(request: Request) {
        when (val effect = request.effect) {
            is Effect.Render -> {
                this.view = ViewModel.bincodeDeserialize(view())
            }

            is Effect.Datetime -> {
                when (val operation = effect.value) {
                    is DatetimeOperation.Instant -> {
                        val res = DatetimeOutput.Instant(SystemClock.elapsedRealtime())
                        val effects = handleResponse(request.uuid.toByteArray(), res.bincodeSerialize())
                        val requests = Requests.bincodeDeserialize(effects)
                        for (request in requests) {
                            processEffect(request)
                        }
                    }

                    is DatetimeOperation.LocalTime -> {
                        val date = System.currentTimeMillis()
                        val offset = TimeZone.getDefault().getOffset(date) / 60000
                        val res = DatetimeOutput.LocalTime(date, offset)

                        val effects = handleResponse(request.uuid.toByteArray(), res.bincodeSerialize())
                        val requests = Requests.bincodeDeserialize(effects)
                        for (request in requests) {
                            processEffect(request)
                        }
                    }

                    is DatetimeOperation.StartTicking -> {
                        ticking.set(true)
                        viewModelScope.launch(Dispatchers.Default) {
                            while (ticking.get()) {
                                delay(operation.value)
                                val res = DatetimeOutput.Instant(SystemClock.elapsedRealtime())
                                val effects = handleResponse(request.uuid.toByteArray(), res.bincodeSerialize())
                                val requests = Requests.bincodeDeserialize(effects)
                                for (request in requests) {
                                    processEffect(request)
                                }
                            }
                        }
                    }

                    is DatetimeOperation.StopTicking -> {
                        ticking.set(false)
                    }
                }
            }

            is Effect.Random -> {
                val res = RandomOutput(rnd.nextLong())
                val effects = handleResponse(request.uuid.toByteArray(), res.bincodeSerialize())
                val requests = Requests.bincodeDeserialize(effects)
                for (request in requests) {
                    processEffect(request)
                }
            }

            is Effect.Storage -> {
                when (val operation = effect.value) {
                    is StorageOperation.Retrieve -> {
                        val res = retrieveFile(operation.filename)
                        val effects = handleResponse(request.uuid.toByteArray(), res.bincodeSerialize())
                        val requests = Requests.bincodeDeserialize(effects)
                        for (request in requests) {
                            processEffect(request)
                        }
                    }

                    is StorageOperation.Replace -> {
                        writeFile(operation.filename, operation.content, false)
                    }

                    is StorageOperation.Delete -> {
                        deleteFile(operation.filename)
                    }
                }
            }
        }
    }

    private suspend fun retrieveFile(filename: String): StorageOutput {
        val ctx = getApplication<Application>().applicationContext;

        try {
            val reader = ctx.openFileInput(filename).bufferedReader();
            val content = reader.readText();
            reader.close();
            return StorageOutput(Optional.of(content));
        } catch (ex: FileNotFoundException) {
            return StorageOutput(Optional.empty());
        }
    }

    private suspend fun writeFile(filename: String, content: String, append: Boolean) {
        val ctx = getApplication<Application>().applicationContext;
        val flags = Context.MODE_PRIVATE or (if (append) Context.MODE_APPEND else 0)

        ctx.openFileOutput(filename, flags).use {
            it.write(content.toByteArray())
        }
    }

    private suspend fun deleteFile(filename: String) {
        val ctx = getApplication<Application>().applicationContext;
        ctx.deleteFile(filename)
    }
}