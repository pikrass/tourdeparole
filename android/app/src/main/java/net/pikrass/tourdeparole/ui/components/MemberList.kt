package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import net.pikrass.tourdeparole.R
import net.pikrass.tourdeparole.tdp_types.MemberView
import java.util.Optional

@Composable
fun MemberList(
    members: List<MemberView>,
    onPrioToggle: (idx: Long) -> Unit,
    onHiddenToggle: (idx: Long) -> Unit,
    onDelete: (idx: Long) -> Unit,
    onAdd: Optional<(String, Boolean) -> Unit>,
) {
    LazyColumn() {
        members.forEach { member ->
            item {
                ListItem(
                    headlineContent = { Text(member.name) },
                    trailingContent = {
                        Row(
                            horizontalArrangement = Arrangement.spacedBy(8.dp),
                        ) {
                            PrioButton(pressed = member.prio, onClick = { onPrioToggle(member.id) })
                            IconButton(onClick = { onHiddenToggle(member.id) }) {
                                if (!member.hidden) {
                                    Icon(painterResource(R.drawable.baseline_visibility_24), contentDescription = "Visible")
                                } else {
                                    Icon(painterResource(R.drawable.baseline_visibility_off_24), contentDescription = "Hidden")
                                }
                            }
                            IconButton(enabled = member.can_delete, onClick = { onDelete(member.id) }) {
                                Icon(Icons.Filled.Delete, contentDescription = "Delete")
                            }
                        }
                    },
                )
            }
            item {
                HorizontalDivider()
            }
        }

        if (onAdd.isPresent) {
            item {
                MemberAdd(modifier = Modifier.padding(5.dp), onAdd = { name, prio -> onAdd.get()(name, prio) })
            }
        }
    }
}