package net.pikrass.tourdeparole.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.Profile
import net.pikrass.tourdeparole.tdp_types.Screen
import net.pikrass.tourdeparole.tdp_types.SessionScreenView
import net.pikrass.tourdeparole.tdp_types.SessionView
import net.pikrass.tourdeparole.ui.components.ProfileSelectDialog
import net.pikrass.tourdeparole.ui.components.TextInputDialog
import java.util.Optional

@Composable
fun ScaffoldTitle(screen: Screen, activeSession: Optional<SessionView>, getSessionName: (Long) -> String?) {
    val default = @Composable { Text("Tour de parole") }

    when (screen) {
        is Screen.Session -> {
            if (activeSession.isPresent) {
                val name = activeSession.map { getSessionName(it.id) }.orElse(null)
                if (name != null) {
                    Text(name)
                } else {
                    default()
                }
            } else {
                default()
            }
        }

        else -> {
            default()
        }
    }
}

@Composable
fun ScaffoldActions(
    screen: Screen,
    profiles: List<Profile>,
    activeSession: Optional<SessionView>,
    update: (Event) -> Unit
) {
    var actionMenuOpen by remember { mutableStateOf(false) }
    var renameOpen by remember { mutableStateOf(false) }
    var profileSelectOpen by remember { mutableStateOf(false) }

    if (renameOpen) {
        TextInputDialog(
            label = "Nouveau nom",
            onDismiss = {
                renameOpen = false
            },
            onConfirm = { name ->
                val trimmedName = name.trim()
                if (trimmedName != "") {
                    val sessId = activeSession.map { it.id }
                    sessId.ifPresent { sessId ->
                        update(Event.EditSessionName(sessId, trimmedName))
                    }
                }
                renameOpen = false
            }
        )
    }

    if (profileSelectOpen) {
        ProfileSelectDialog(
            profiles = profiles,
            selected = activeSession.get().profile_id,
            onChange = { update(Event.ChangeSessionProfile(it)) },
            onDismiss = { profileSelectOpen = false },
        )
    }

    when (screen) {
        is Screen.Session -> {
            when (screen.value) {
                is SessionScreenView.ActiveSession -> {
                    Box {
                        IconButton(
                            onClick = { actionMenuOpen = true }
                        ) {
                            Icon(Icons.Filled.MoreVert, contentDescription = "Actions")
                        }
                        DropdownMenu(
                            expanded = actionMenuOpen,
                            onDismissRequest = { actionMenuOpen = false },
                        ) {
                            DropdownMenuItem(
                                text = { Text("Réinitialiser") },
                                onClick = {
                                    actionMenuOpen = false
                                    update(Event.NewSubsession())
                                }
                            )
                            DropdownMenuItem(
                                text = { Text("Nouvelle session") },
                                onClick = {
                                    actionMenuOpen = false
                                    val profile = activeSession.map { it.profile_id }
                                    update(Event.NewSession(profile))
                                }
                            )
                            DropdownMenuItem(
                                text = { Text("Renommer la session") },
                                onClick = {
                                    actionMenuOpen = false
                                    renameOpen = true
                                }
                            )
                            DropdownMenuItem(
                                text = { Text("Changer de profil") },
                                onClick = {
                                    actionMenuOpen = false
                                    profileSelectOpen = true
                                }
                            )
                            DropdownMenuItem(
                                text = { Text("Gérer les membres") },
                                onClick = {
                                    actionMenuOpen = false
                                    update(Event.NavEditMembers())
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}
