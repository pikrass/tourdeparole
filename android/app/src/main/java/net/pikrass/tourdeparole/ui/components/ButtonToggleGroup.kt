/**
 *  Composable Button Toggle Group©
 *  Copyright 2022 Robert Levonyan
 *  Url: https://github.com/robertlevonyan/composable-button-toggle-group
 *
 *  MODIFIED by Chloé Scher, 2024
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ButtonElevation
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun RowToggleButtonGroup(
  modifier: Modifier = Modifier,
  buttonCount: Int,
  selection: Int = -1,
  selectedColor: Color = Color.White,
  unselectedColor: Color = Color.Unspecified,
  selectedContentColor: Color = Color.Black,
  unselectedContentColor: Color = Color.Gray,
  buttonIconTint: Color = selectedContentColor,
  unselectedButtonIconTint: Color = unselectedContentColor,
  borderColor: Color = selectedColor,
  buttonTexts: Array<String> = Array(buttonCount) { "" },
  buttonIcons: Array<Painter> = Array(buttonCount) { ColorPainter(Color.Transparent) },
  shape: CornerBasedShape = MaterialTheme.shapes.small,
  borderSize: Dp = 1.dp,
  border: BorderStroke = BorderStroke(borderSize, borderColor),
  defaultElevation: Dp = 0.dp,
  pressedElevation: Dp = 0.dp,
  enabled: Boolean = true,
  buttonHeight: Dp = 60.dp,
  iconPosition: IconPosition = IconPosition.Start,
  onButtonClick: (index: Int) -> Unit,
) {
  Row(modifier = modifier.horizontalScroll(rememberScrollState())) {
    val squareCorner = CornerSize(0.dp)

    repeat(buttonCount) { index ->
      val buttonShape = when (index) {
        0 -> shape.copy(bottomEnd = squareCorner, topEnd = squareCorner)
        buttonCount - 1 -> shape.copy(topStart = squareCorner, bottomStart = squareCorner)
        else -> shape.copy(all = squareCorner)
      }
      val isButtonSelected = selection == index
      val backgroundColor = if (isButtonSelected) selectedColor else unselectedColor
      val contentColor = if (isButtonSelected) selectedContentColor else unselectedContentColor
      val iconTintColor = if (isButtonSelected) buttonIconTint else unselectedButtonIconTint
      val offset = borderSize * -index

      ToggleButton(
        modifier = Modifier
          .defaultMinSize(minHeight = buttonHeight)
          .offset(x = offset),
        buttonShape = buttonShape,
        border = border,
        backgroundColor = backgroundColor,
        defaultElevation = defaultElevation,
        pressedElevation = pressedElevation,
        enabled = enabled,
        pressed = isButtonSelected,
        buttonTexts = buttonTexts,
        buttonIcons = buttonIcons,
        index = index,
        contentColor = contentColor,
        iconTintColor = iconTintColor,
        iconPosition = iconPosition,
        onClick = {
          onButtonClick.invoke(index)
        },
      )
    }
  }
}

@Composable
private inline fun ToggleButton(
  modifier: Modifier,
  buttonShape: CornerBasedShape,
  border: BorderStroke,
  backgroundColor: Color,
  defaultElevation: Dp,
  pressedElevation: Dp,
  enabled: Boolean,
  pressed: Boolean,
  buttonTexts: Array<String>,
  buttonIcons: Array<Painter>,
  index: Int,
  contentColor: Color,
  iconTintColor: Color,
  iconPosition: IconPosition,
  crossinline onClick: () -> Unit,
) {
  OutlinedButton(
    modifier = modifier,
    contentPadding = PaddingValues(),
    shape = buttonShape,
    border = border,
    onClick = { onClick() },
    colors = ButtonDefaults.outlinedButtonColors(containerColor = backgroundColor),
    elevation = if (pressed) {
        ButtonDefaults.buttonElevation(
          defaultElevation = pressedElevation,
          pressedElevation = pressedElevation,
        )
      } else {
        ButtonDefaults.buttonElevation(
          defaultElevation = defaultElevation,
          pressedElevation = pressedElevation,
        )
      },
    enabled = enabled,
  ) {
    ButtonContent(
      buttonTexts = buttonTexts,
      buttonIcons = buttonIcons,
      index = index,
      contentColor = contentColor,
      iconTintColor = iconTintColor,
      iconPosition = iconPosition,
    )
  }
}

@Composable
private fun RowScope.ButtonContent(
  buttonTexts: Array<String>,
  buttonIcons: Array<Painter>,
  index: Int,
  contentColor: Color,
  iconTintColor: Color,
  iconPosition: IconPosition = IconPosition.Start,
) {
  when {
    buttonTexts.all { it != "" } && buttonIcons.all { it != emptyPainter } -> ButtonWithIconAndText(
      iconTintColor = iconTintColor,
      buttonIcons = buttonIcons,
      buttonTexts = buttonTexts,
      index = index,
      contentColor = contentColor,
      iconPosition = iconPosition,
    )

    buttonTexts.all { it != "" } && buttonIcons.all { it == emptyPainter } -> TextContent(
      modifier = Modifier.align(Alignment.CenterVertically),
      buttonTexts = buttonTexts,
      index = index,
      contentColor = contentColor,
    )

    buttonTexts.all { it == "" } && buttonIcons.all { it != emptyPainter } -> IconContent(
      modifier = Modifier.align(Alignment.CenterVertically),
      iconTintColor = iconTintColor,
      buttonIcons = buttonIcons,
      index = index,
    )
  }
}

@Composable
private fun RowScope.ButtonWithIconAndText(
  iconTintColor: Color,
  buttonIcons: Array<Painter>,
  buttonTexts: Array<String>,
  index: Int,
  contentColor: Color,
  iconPosition: IconPosition,
) {
  when (iconPosition) {
    IconPosition.Start -> {
      IconContent(Modifier.align(Alignment.CenterVertically), iconTintColor, buttonIcons, index)
      TextContent(Modifier.align(Alignment.CenterVertically), buttonTexts, index, contentColor)
    }

    IconPosition.Top -> Column {
      IconContent(Modifier.align(Alignment.CenterHorizontally), iconTintColor, buttonIcons, index)
      TextContent(Modifier.align(Alignment.CenterHorizontally), buttonTexts, index, contentColor)
    }

    IconPosition.End -> {
      TextContent(Modifier.align(Alignment.CenterVertically), buttonTexts, index, contentColor)
      IconContent(Modifier.align(Alignment.CenterVertically), iconTintColor, buttonIcons, index)
    }

    IconPosition.Bottom -> Column {
      TextContent(Modifier.align(Alignment.CenterHorizontally), buttonTexts, index, contentColor)
      IconContent(Modifier.align(Alignment.CenterHorizontally), iconTintColor, buttonIcons, index)
    }
  }
}

@Composable
private fun IconContent(
  modifier: Modifier,
  iconTintColor: Color,
  buttonIcons: Array<Painter>,
  index: Int,
) {
  if (iconTintColor == Color.Transparent || iconTintColor == Color.Unspecified) {
    Image(
      modifier = modifier.size(24.dp),
      painter = buttonIcons[index],
      contentDescription = null,
    )
  } else {
    Image(
      modifier = modifier.size(24.dp),
      painter = buttonIcons[index],
      contentDescription = null,
      colorFilter = ColorFilter.tint(iconTintColor),
    )
  }
}

@Composable
private fun TextContent(
  modifier: Modifier,
  buttonTexts: Array<String>,
  index: Int,
  contentColor: Color,
) {
  Text(
    modifier = modifier.padding(horizontal = 8.dp),
    text = buttonTexts[index],
    color = contentColor,
    maxLines = 1,
    overflow = TextOverflow.Ellipsis,
  )
}

private val emptyPainter = ColorPainter(Color.Transparent)

enum class IconPosition {
  Start, Top, End, Bottom
}
