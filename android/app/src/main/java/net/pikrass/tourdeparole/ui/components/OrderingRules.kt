package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectVerticalDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.OutlinedCard
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.PointerId
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import net.pikrass.tourdeparole.R
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.OrderRule
import net.pikrass.tourdeparole.tdp_types.OrderRuleData
import kotlin.math.roundToInt

@Composable
fun OrderingRules(
    profileId: Long,
    orderRules: List<OrderRule>,
    availOrderRules: List<OrderRuleData>,
    update: (Event) -> Unit,
) {
    val profileRules by rememberUpdatedState(newValue = orderRules)
    val availableRules = availOrderRules.toMutableSet()
    val rulesPos: MutableMap<Int, Float> = remember { mutableMapOf() }

    var rulesDragY by remember { mutableFloatStateOf(0f) }
    var rulesDragCurrent: Pair<PointerId, OrderRule>? by remember { mutableStateOf(null) }

    OutlinedCard(
        modifier = Modifier.pointerInput(Unit) {
            detectVerticalDragGestures(
                onVerticalDrag = { change, offset ->
                    rulesDragCurrent?.let { cur ->
                        if (cur.first != change.id)
                            return@let

                        change.consume()
                        rulesDragY += offset

                        val index = profileRules.indexOf(cur.second)
                        val ourInitialPos = rulesPos.getValue(index)

                        rulesPos[index - 1]?.let { prevPos ->
                            if (ourInitialPos + rulesDragY < prevPos) {
                                update(Event.MoveOrderRule(profileId, cur.second, -1))
                                rulesDragY += ourInitialPos - prevPos
                            }
                        }

                        rulesPos[index + 1]?.let { nextPos ->
                            if (ourInitialPos + rulesDragY > nextPos) {
                                update(Event.MoveOrderRule(profileId, cur.second, +1))
                                rulesDragY -= nextPos - ourInitialPos
                            }
                        }
                    }
                },
                onDragEnd = {
                    rulesDragCurrent = null
                    rulesDragY = 0f
                }
            )
        }
    ) {
        orderRules.forEachIndexed { index, rule ->
            val data = availOrderRules.find { it.rule == rule } ?: return@forEachIndexed
            availableRules.remove(data)

            var baseModifier = Modifier
                .padding(vertical = 2.dp)

            if (!data.must_be_last) {
                baseModifier = baseModifier
                    .onGloballyPositioned { coords ->
                        rulesPos[index] = coords.positionInParent().y + coords.size.height / 2
                    }
            }

            ListItem(
                modifier = baseModifier
                    .offset { IntOffset(0, rulesDragY.roundToInt()) }
                    .zIndex(1f)
                    .takeIf { rulesDragCurrent?.second == rule } ?: baseModifier,
                shadowElevation = 2.dp,
                headlineContent = { Text("${index + 1}. ${data.desc}") },
                trailingContent = (@Composable() {
                    Row {
                        Icon(
                            Icons.Filled.Delete,
                            contentDescription = "Delete rule",
                            modifier = Modifier.clickable(
                                onClick = {
                                    update(Event.RemoveOrderRule(profileId, rule))
                                    rulesPos.clear()
                                }
                            ),
                        )
                        Icon(
                            painterResource(R.drawable.baseline_drag_handle_24),
                            contentDescription = "Reorder rules",
                            modifier = Modifier
                                .pointerInput(key1 = rule) {
                                    awaitPointerEventScope {
                                        while(true) {
                                            val event = awaitPointerEvent()
                                            event.changes.forEach { change ->
                                                if (change.pressed && !change.previousPressed)
                                                    rulesDragCurrent = Pair(change.id, rule)
                                            }
                                        }
                                    }
                                }
                        )
                    }
                }).takeIf { !data.must_be_last }
            )
        }
        Box(
            modifier = Modifier.align(Alignment.End)
        ) {
            var showMenu by remember { mutableStateOf(false) }
            IconButton(
                enabled = availableRules.isNotEmpty(),
                onClick = { showMenu = !showMenu },
            ) {
                Icon(Icons.Filled.Add, contentDescription = "Add a rule")
            }
            DropdownMenu(
                expanded = showMenu,
                onDismissRequest = { showMenu = false },
            ) {
                availableRules.forEach { ruleData ->
                    DropdownMenuItem(
                        text = { Text(ruleData.desc) },
                        onClick = {
                            update(Event.AddOrderRule(profileId, ruleData.rule))
                            showMenu = false
                        }
                    )
                }
            }
        }
    }
}