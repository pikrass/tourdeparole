package net.pikrass.tourdeparole.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.clickable
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import java.util.Optional
import kotlin.collections.asReversed
import net.pikrass.tourdeparole.ui.components.PrioStatsComparator
import net.pikrass.tourdeparole.ui.components.RowToggleButtonGroup
import net.pikrass.tourdeparole.tdp_types.SubsessionStats
import net.pikrass.tourdeparole.tdp_types.SessionStats
import net.pikrass.tourdeparole.tdp_types.MemberStats
import net.pikrass.tourdeparole.R

@Composable
fun SessionOverview(stats: SessionStats) {
    var subsession by remember { mutableIntStateOf(-1) }

    Column(
        modifier = Modifier.padding(10.dp),
    ) {
        if (stats.subsessions.count() > 1) {
            Text("Sous-sessions", fontWeight = FontWeight.Bold)

            val labels = (1..stats.subsessions.count()).map { it.toString() }

            RowToggleButtonGroup(
                buttonCount = stats.subsessions.count() + 1,
                buttonTexts = arrayOf("Toutes", *labels.toTypedArray()),

                buttonHeight = 30.dp,
                defaultElevation = 2.dp,
                pressedElevation = 0.dp,
                selectedColor = Color.White,
                unselectedColor = Color.LightGray,

                selection = subsession + 1,
                onButtonClick = { index -> subsession = index - 1 },

                modifier = Modifier.padding(bottom = 4.dp),
            )
        }

        SubsessionStats(
            stats = if (subsession < 0) stats.totals else stats.subsessions[subsession]
        )
    }
}

@Composable
private fun SubsessionStats(stats: SubsessionStats) {
    Column {
        if (stats.prio.isPresent) {
            Text(
                "Statistiques collectives",
                fontWeight = FontWeight.Bold,
                modifier = Modifier.padding(bottom = 4.dp),
            )
            PrioStatsComparator(
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(bottom = 16.dp),
                nbNonprio = stats.prio.get().members[0],
                nbPrio = stats.prio.get().members[1],
                speechesNonprio = stats.prio.get().speeches[0].toLong(),
                speechesPrio = stats.prio.get().speeches[1].toLong(),
                timeNonprio = stats.prio.get().time[0],
                timePrio = stats.prio.get().time[1],
            )
        }

        Text(
            "Statistiques individuelles",
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 8.dp),
        )
        MemberStatList(stats.members)
    }
}

@Composable
private fun MemberStatList(members: List<MemberStats>) {
    val nbMems = members.size.toLong()
    val totSpeeches = members.map { it.speeches }.sum().toLong()
    val totTime = members.map { it.time }.sum().toLong()

    var sortKey by remember { mutableStateOf("name") }
    var sortReverse by remember { mutableStateOf(false) }

    val sortIcon: @Composable () -> Unit = {
        if (sortReverse) {
            Icon(Icons.Filled.KeyboardArrowUp, contentDescription = "")
        } else {
            Icon(Icons.Filled.KeyboardArrowDown, contentDescription = "")
        }
    }

    LazyColumn(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.spacedBy(4.dp),
    ) {
        item {
            Row(
                modifier = Modifier.padding(bottom = 4.dp),
            ) {
                Row(
                    modifier = Modifier.weight(.4f).clickable {
                        if (sortKey == "name") {
                            sortReverse = !sortReverse
                        } else {
                            sortKey = "name"
                            sortReverse = false
                        }
                    },
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(Icons.Filled.Person, contentDescription = "")
                    Text("Nom", fontWeight = FontWeight.Bold)
                    if (sortKey == "name")
                        sortIcon()
                }

                Row(
                    modifier = Modifier.weight(.3f).clickable {
                        if (sortKey == "speeches") {
                            sortReverse = !sortReverse
                        } else {
                            sortKey = "speeches"
                            sortReverse = false
                        }
                    },
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(painterResource(R.drawable.baseline_mic_24), contentDescription = "")
                    Text("Prises", fontWeight = FontWeight.Bold)
                    if (sortKey == "speeches")
                        sortIcon()
                }

                Row(
                    modifier = Modifier.weight(.3f).clickable {
                        if (sortKey == "time") {
                            sortReverse = !sortReverse
                        } else {
                            sortKey = "time"
                            sortReverse = false
                        }
                    },
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Icon(painterResource(R.drawable.baseline_access_time_24), contentDescription = "")
                    Text("Durée", fontWeight = FontWeight.Bold)
                    if (sortKey == "time")
                        sortIcon()
                }
            }
            HorizontalDivider()
        }

        when (sortKey) {
            "name" -> members.sortedBy { it.name }
            "speeches" -> members.sortedBy { it.speeches }
            "time" -> members.sortedBy { it.time }
            else -> members.sortedBy { it.name }
        }.let {
            if (sortReverse)
                it.asReversed()
            else
                it
        }.forEach { mem ->
            item(mem.name) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    val color = if (mem.prio) {
                        Color(red = 204, green = 113, blue = 16)
                    } else {
                        Color(red = 16, green = 16, blue = 204)
                    }

                    Text(
                        mem.name,
                        modifier = Modifier.weight(.4f),
                        textAlign = TextAlign.Center,
                        color = color,
                    )
                    Stat(
                        str = mem.speeches.toString(),
                        value = mem.speeches.toLong(),
                        total = totSpeeches,
                        nb = nbMems,
                        color = color,
                        modifier = Modifier.weight(.3f),
                    )
                    Stat(
                        str = "%d:%02d".format(mem.time / 60000, (mem.time % 60000) / 1000),
                        value = mem.time.toLong(),
                        total = totTime,
                        nb = nbMems,
                        color = color,
                        modifier = Modifier.weight(.3f),
                    )
                }
                HorizontalDivider()
            }
        }
    }
}

@Composable
private fun Stat(
    str: String,
    value: Long,
    total: Long,
    nb: Long,
    color: Color = Color.Unspecified,
    modifier: Modifier = Modifier,
) {
    val big = 17.sp
    val small = 9.sp
    val percent = value.toDouble() / total.toDouble() * 100f
    val fraction = value.toDouble() / total.toDouble() * nb.toDouble()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier,
    ) {
        Text(
            str,
            textAlign = TextAlign.Center,
            color = color,
            fontSize = big,
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text(
                "%.0f%%".format(percent),
                textAlign = TextAlign.Center,
                color = color,
                fontSize = small,
            )
            Spacer(
                modifier = Modifier.requiredWidth(10.dp),
            )
            Text(
                "%.1f".format(fraction),
                textAlign = TextAlign.Center,
                color = color,
                fontSize = small,
            )
            Icon(
                Icons.Filled.Person,
                modifier = Modifier.size(11.dp),
                tint = color,
                contentDescription = "",
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun MemberStatListPreview() {
    val members = listOf(
        MemberStats ("Martin", false, 3, 133000),
        MemberStats ("Lucie", true, 2, 57000),
    )
    MemberStatList(members = members)
}
