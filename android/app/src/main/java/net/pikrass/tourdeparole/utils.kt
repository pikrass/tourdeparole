package net.pikrass.tourdeparole

import net.pikrass.tourdeparole.tdp_types.DateTimeWithOffset
import java.text.DateFormat
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import java.util.SimpleTimeZone

fun DateTimeWithOffset.formatFull(): String {
    val date = Date(this.datetime + this.offset)
    return DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT)
        .format(date)
        .replaceFirstChar { it.uppercase() }
}

fun DateTimeWithOffset.formatTime(): String {
    val date = Date(this.datetime + this.offset)
    return DateFormat.getTimeInstance(DateFormat.SHORT).format(date)
}

fun DateTimeWithOffset.sameDay(other: DateTimeWithOffset): Boolean {
    val tz1 = SimpleTimeZone(this.offset * 60000, "UTC%+d:%02d".format(this.offset / 60, this.offset % 60))
    val tz2 = SimpleTimeZone(other.offset * 60000, "UTC%+d:%02d".format(other.offset / 60, other.offset % 60))
    val myDate = GregorianCalendar(tz1)
    myDate.time = Date(this.datetime)
    val otherDate = GregorianCalendar(tz2)
    otherDate.time = Date(other.datetime)

    return myDate.get(Calendar.YEAR) == otherDate.get(Calendar.YEAR)
            && myDate.get(Calendar.MONTH) == otherDate.get(Calendar.MONTH)
            && myDate.get(Calendar.DAY_OF_MONTH) == otherDate.get(Calendar.DAY_OF_MONTH)
}