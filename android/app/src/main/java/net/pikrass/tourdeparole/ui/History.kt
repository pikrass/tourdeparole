package net.pikrass.tourdeparole.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import net.pikrass.tourdeparole.formatFull
import net.pikrass.tourdeparole.formatTime
import net.pikrass.tourdeparole.sameDay
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.SessionIndexEntry
import net.pikrass.tourdeparole.ui.components.TextInputDialog
import java.util.Optional

@Composable
fun History(sessions: List<SessionIndexEntry>, update: (Event) -> Unit, getProfileName: (Long) -> String?) {
    val iconMod = Modifier
        .size(16.dp)
        .padding(end = 3.dp)
    val attrFontSize = 14.sp

    var menuOpen: Long? by remember { mutableStateOf(null) }
    var renameOpen: Long? by remember { mutableStateOf(null) }

    if (renameOpen != null) {
        TextInputDialog(
            label = "Nouveau nom",
            onDismiss = {
                renameOpen = null
            },
            onConfirm = { name ->
                val trimmedName = name.trim()
                if (trimmedName != "") {
                    renameOpen.let { renamed ->
                        update(Event.EditSessionName(renamed, trimmedName))
                    }
                }
                renameOpen = null
            }
        )
    }

    LazyColumn(
        contentPadding = PaddingValues(horizontal = 8.dp, vertical = 16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
    ) {
        sessions.forEach { session ->
            item {
                ElevatedCard(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(5.dp),
                    elevation = CardDefaults.cardElevation(defaultElevation = 4.dp),
                ) {
                    Column(
                        modifier = Modifier.padding(5.dp),
                        verticalArrangement = Arrangement.spacedBy(2.dp),
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Text(
                                text = session.name,
                                fontSize = 20.sp,
                                maxLines = 1,
                                overflow = TextOverflow.Ellipsis,
                                modifier = Modifier.weight(1f),
                            )
                            Box {
                                IconButton(
                                    onClick = {
                                        menuOpen = if (menuOpen == session.id) { null } else { session.id }
                                    }
                                ) {
                                    Icon(Icons.Filled.MoreVert, contentDescription = "Session menu")
                                }
                                DropdownMenu(
                                    expanded = menuOpen == session.id,
                                    onDismissRequest = { menuOpen = null },
                                ) {
                                    DropdownMenuItem(
                                        text = { Text("Reprendre") },
                                        onClick = {
                                            menuOpen = null
                                            update(Event.OpenSession(Optional.of(session.id)))
                                        }
                                    )
                                    DropdownMenuItem(
                                        text = { Text("Statistiques") },
                                        onClick = {
                                            menuOpen = null
                                            update(Event.NavSessionStats(session.id))
                                        }
                                    )
                                    DropdownMenuItem(
                                        text = { Text("Renommer") },
                                        onClick = {
                                            renameOpen = menuOpen
                                            menuOpen = null
                                        }
                                    )
                                    DropdownMenuItem(
                                        text = { Text("Supprimer") },
                                        onClick = {
                                            menuOpen = null
                                            update(Event.DeleteSession(session.id))
                                        }
                                    )
                                }
                            }
                        }

                        Row(
                            verticalAlignment = Alignment.CenterVertically,
                        ) {
                            Icon(Icons.Filled.DateRange, contentDescription = "Date", modifier = iconMod)
                            if (session.started_at.isPresent) {
                                val startedAt = session.started_at.get()
                                val updatedAt = session.updated_at.get()

                                if (startedAt.sameDay(updatedAt)) {
                                    Text("${startedAt.formatFull()} - ${updatedAt.formatTime()}", fontSize = attrFontSize)
                                } else {
                                    Column {
                                        Text(startedAt.formatFull(), fontSize = attrFontSize)
                                        Text(updatedAt.formatFull(), fontSize = attrFontSize)
                                    }
                                }
                            } else {
                                Text("Pas commencée", fontSize = attrFontSize)
                            }
                        }
                        Row(
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Icon(Icons.Filled.Settings, contentDescription = "Profil", modifier = iconMod)
                            Text(getProfileName(session.profile_id) ?: "(Supprimé)", fontSize = attrFontSize)
                        }
                    }
                }
            }
        }
    }
}
