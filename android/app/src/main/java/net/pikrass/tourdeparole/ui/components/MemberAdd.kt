package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MemberAdd(onAdd: (String, Boolean) -> Unit, modifier: Modifier = Modifier) {
    Row(
        modifier = modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        var name by remember { mutableStateOf("") }
        var prio by remember { mutableStateOf(false) }

        TextField(
            modifier = Modifier
                .padding(horizontal = 5.dp)
                .weight(1f),
            value = name,
            onValueChange = { name = it },
            singleLine = true,
            keyboardActions = KeyboardActions(
                onDone = { onAdd(name, prio); name = "" },
            ),
            label = { Text("Nouveau·elle") }
        )
        PrioButton(pressed = prio, onClick = { prio = !prio })
        IconButton(onClick = { onAdd(name, prio); name = "" }) {
            Icon(Icons.Filled.Add, contentDescription = "Add a member")
        }
    }
}