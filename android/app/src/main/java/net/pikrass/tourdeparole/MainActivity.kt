@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalAnimationApi::class)

package net.pikrass.tourdeparole

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.animation.slideOutVertically
import androidx.compose.animation.with
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.*
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.Screen
import net.pikrass.tourdeparole.tdp_types.ScreenCat
import net.pikrass.tourdeparole.tdp_types.ProfileScreenView
import net.pikrass.tourdeparole.tdp_types.SessionScreenView
import net.pikrass.tourdeparole.tdp_types.HistoryScreenView
import net.pikrass.tourdeparole.ui.EditMembers
import net.pikrass.tourdeparole.ui.EditProfile
import net.pikrass.tourdeparole.ui.History
import net.pikrass.tourdeparole.ui.ProfileList
import net.pikrass.tourdeparole.ui.ScaffoldActions
import net.pikrass.tourdeparole.ui.ScaffoldTitle
import net.pikrass.tourdeparole.ui.SessionOverview
import net.pikrass.tourdeparole.ui.SessionView
import net.pikrass.tourdeparole.ui.theme.TourDeParoleTheme
import kotlin.jvm.optionals.getOrNull

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContent {
            TourDeParoleTheme {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    TdpApp()
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()

        val core: Core by viewModels()
        CoroutineScope(Dispatchers.IO).launch {
            core.update(Event.Stop())
        }
    }
}

@Composable
fun TdpApp(core: Core = viewModel()) {
    val coroutineScope = rememberCoroutineScope()

    val update: (Event) -> Unit = { event ->
        coroutineScope.launch {
            core.update(event)
        }
    }

    val getProfileName: (Long) -> String? = { id ->
        core.view.profiles.find { it.id == id }?.name
    }

    val getSessionName: (Long) -> String? = { id ->
        core.view.sessions.find { it.id == id }?.name
    }

    LaunchedEffect(coroutineScope) {
        core.update(Event.Start())
    }

    Scaffold(
        modifier = Modifier.imePadding(),
        topBar = {
            TopAppBar(
                colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer,
                    titleContentColor = MaterialTheme.colorScheme.primary,
                ),
                title = { ScaffoldTitle(core.view.screen, core.view.active_session, getSessionName) },
                navigationIcon = {
                    if (core.view.show_back_button) {
                        IconButton(onClick = { update(Event.NavBack()) }) {
                            Icon(
                                Icons.AutoMirrored.Filled.ArrowBack,
                                contentDescription = "Retour",
                            )
                        }
                    }
                },
                actions = {
                    ScaffoldActions(
                        screen = core.view.screen,
                        profiles = core.view.profiles,
                        activeSession = core.view.active_session,
                        update = update,
                    )
                },
            )
        },
        floatingActionButton = {
            when (val screen = core.view.screen) {
                is Screen.Profile -> {
                    when (screen.value) {
                        is ProfileScreenView.ProfileList -> {
                            FloatingActionButton(onClick = { update(Event.NewProfile()) }) {
                                Icon(Icons.Filled.Add, contentDescription = "New profile")
                            }
                        }
                    }
                }
            }
        },
        bottomBar = {
            NavigationBar {
                NavigationBarItem(
                    icon = { Icon(Icons.AutoMirrored.Filled.List, contentDescription = "Profils")},
                    label = { Text("Profils") },
                    selected = core.view.screen is Screen.Profile,
                    onClick = { update(Event.NavCat(ScreenCat.Profile())) },
                )
                NavigationBarItem(
                    icon = { Icon(painterResource(R.drawable.baseline_forum_24), contentDescription = "Session")},
                    label = { Text("Session") },
                    selected = core.view.screen is Screen.Session,
                    onClick = { update(Event.NavCat(ScreenCat.Session())) },
                )
                NavigationBarItem(
                    icon = { Icon(painterResource(R.drawable.baseline_history_24), contentDescription = "Historique")},
                    label = { Text("Historique") },
                    selected = core.view.screen is Screen.History,
                    onClick = { update(Event.NavCat(ScreenCat.History())) },
                )
            }
        }
    ) { innerPadding ->
        AnimatedContent(
            targetState = core.view.screen,
            modifier = Modifier.padding(innerPadding),
            label = "MainContent",
            transitionSpec = {
                val screenPos: (Screen) -> Int = { screen ->
                    when (screen) {
                        is Screen.Profile -> 0
                        is Screen.Session -> 1
                        is Screen.History -> 2
                        else -> 3
                    }
                }

                val subscreenPos: (Screen) -> Int = { screen ->
                    when (screen) {
                        is Screen.Profile -> when (screen.value) {
                            is ProfileScreenView.ProfileList -> 0
                            is ProfileScreenView.EditProfile -> 1
                            else -> 2
                        }
                        is Screen.Session -> when (screen.value) {
                            is SessionScreenView.ActiveSession -> 0
                            is SessionScreenView.EditMembers -> 1
                            else -> 2
                        }
                        is Screen.History -> 0
                        else -> 0
                    }
                }

                val initialPos = screenPos(initialState)
                val targetPos = screenPos(targetState)

                if (initialPos < targetPos) {
                    slideInHorizontally { width -> width } + fadeIn() with
                            slideOutHorizontally { width -> -width } + fadeOut()
                } else if (initialPos > targetPos) {
                    slideInHorizontally { width -> -width } + fadeIn() with
                            slideOutHorizontally { width -> width } + fadeOut()
                } else {
                    if (subscreenPos(initialState) < subscreenPos(targetState)) {
                        slideInVertically { height -> -height } + fadeIn() with
                                slideOutVertically { height -> height } + fadeOut()
                    } else {
                        slideInVertically { height -> height } + fadeIn() with
                                slideOutVertically { height -> -height } + fadeOut()

                    }
                }
            }
        ) { cat ->
            when (cat) {
                is Screen.Profile -> when (val screen = cat.value) {
                    is ProfileScreenView.ProfileList -> {
                        ProfileList(core.view.profiles, update)
                    }

                    is ProfileScreenView.EditProfile -> {
                        val profile = core.view.profiles.find { p ->
                            p.id.equals(screen.value)
                        }

                        if (profile != null) {
                            EditProfile(profile, core.view.available_order_rules, update)
                        }
                    }
                }

                is Screen.Session -> when (cat.value) {
                    is SessionScreenView.ActiveSession -> {
                        val session = core.view.active_session.getOrNull()
                        if (session != null) {
                            SessionView(session, update)
                        }
                    }

                    is SessionScreenView.EditMembers -> {
                        val session = core.view.active_session.getOrNull()
                        if (session != null) {
                            EditMembers(session.members, update)
                        }
                    }
                }

                is Screen.History -> when (val screen = cat.value) {
                    is HistoryScreenView.SessionList -> {
                        History(core.view.sessions, update, getProfileName)
                    }

                    is HistoryScreenView.SessionOverview -> {
                        SessionOverview(screen.value)
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun TdpPreview() {
    TourDeParoleTheme {
        TdpApp()
    }
}
