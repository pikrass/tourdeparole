@file:OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)

package net.pikrass.tourdeparole.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.PressInteraction
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.GenericShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowForward
import androidx.compose.material.icons.automirrored.filled.KeyboardArrowRight
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilterChip
import androidx.compose.material3.FilterChipDefaults
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ProgressIndicatorDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalViewConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import net.pikrass.tourdeparole.R
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.MemberView
import net.pikrass.tourdeparole.tdp_types.SessionView
import net.pikrass.tourdeparole.ui.components.MemberAdd
import net.pikrass.tourdeparole.ui.components.MemberList
import net.pikrass.tourdeparole.ui.components.PrioStatsComparator
import java.util.Optional
import kotlin.jvm.optionals.getOrNull
import kotlin.math.ceil

@Composable
fun SessionView(data: SessionView, update: (Event) -> Unit, modifier: Modifier = Modifier) {
    val viewConfig = LocalViewConfiguration.current
    var showStats by remember { mutableStateOf(false) }

    Column(
        modifier = modifier.padding(10.dp),
    ) {
        SpeakerBar(data, update)
        Row(
            modifier = Modifier.padding(vertical = 5.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Text("%d:%02d".format(data.timer / 60000, (data.timer % 60000) / 1000))
            LinearProgressIndicator(
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 8.dp),
                progress = { data.time_limit.map { limit -> data.timer.toFloat() / limit }.orElse(0f) },
                color = if (data.time_limit.map { limit -> data.timer >= limit }.orElse(false)) {
                    MaterialTheme.colorScheme.error
                } else {
                    ProgressIndicatorDefaults.linearColor
                },
            )
            if (data.time_limit.isPresent) {
                val remaining = ceil((data.time_limit.get() - data.timer) / 1000f).toLong().coerceAtLeast(0)
                Text("%d:%02d".format(remaining / 60, remaining % 60))
            }
        }
        FlowRow(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .verticalScroll(rememberScrollState()),
            horizontalArrangement = Arrangement.spacedBy(5.dp),
            verticalArrangement = Arrangement.Top,
        ) {
            data.members.filter { m -> !m.hidden }.forEach { m ->
                val interactionSource = remember { MutableInteractionSource() }

                LaunchedEffect(m.id) {
                    var longPress = false

                    interactionSource.interactions.collectLatest { interaction ->
                        when (interaction) {
                            is PressInteraction.Press -> {
                                longPress = false
                                delay(viewConfig.longPressTimeoutMillis)
                                longPress = true
                                update(Event.ForceSpeaker(m.id))
                            }

                            is PressInteraction.Release -> {
                                if (!longPress) {
                                    update(Event.ToggleAskToSpeak(m.id))
                                }
                            }
                        }
                    }
                }

                FilterChip(
                    modifier = Modifier.padding(0.dp),
                    interactionSource = interactionSource,
                    label = {
                        Text(m.name)
                        if (m.speeches > 0) {
                            Text(
                                m.speeches.toString(),
                                fontSize = 10.sp,
                                modifier = modifier
                                    .padding(start = 3.dp)
                                    .background(
                                        color = if (m.prio) {
                                            Color(red = 252, green = 140, blue = 20, alpha = 50)
                                        } else {
                                            MaterialTheme.colorScheme.surfaceVariant
                                        },
                                        shape = RoundedCornerShape(percent = 100),
                                    )
                            )
                        }
                    },
                    colors = if (m.prio) {
                        FilterChipDefaults.filterChipColors(
                            containerColor = Color(red = 252, green = 140, blue = 20, alpha = 50),
                            selectedContainerColor = Color(red = 252, green = 140, blue = 20, alpha = 150),
                        )
                    } else {
                        FilterChipDefaults.filterChipColors()
                    },
                    selected = m.asking_to_speak,
                    onClick = { },
                )
            }
        }

        HorizontalDivider()

        Row(modifier = Modifier
            .padding(5.dp)
            .clickable(enabled = data.stats_prio.isPresent) {
                showStats = !showStats
            }
        ) {
            Text("Statistiques collectives",
                modifier = Modifier.weight(1f),
                color = if (data.stats_prio.isPresent) Color.Unspecified
                        else MaterialTheme.colorScheme.onSurface.copy(alpha = 0.5f),
            )
            if (!data.stats_prio.isPresent) {
                Icon(
                    Icons.AutoMirrored.Filled.KeyboardArrowRight,
                    contentDescription = "Stats unavailable",
                    tint = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.5f)
                )
            } else if (!showStats) {
                Icon(Icons.AutoMirrored.Filled.KeyboardArrowRight, contentDescription = "Show stats")
            } else {
                Icon(Icons.Filled.KeyboardArrowDown, contentDescription = "Hide stats")
            }
        }

        if (showStats && data.stats_prio.isPresent) {
            PrioStatsComparator(
                nbNonprio = data.stats_prio.get().members[0],
                nbPrio = data.stats_prio.get().members[1],
                speechesNonprio = data.stats_prio.get().speeches[0].toLong(),
                speechesPrio = data.stats_prio.get().speeches[1].toLong(),
                timeNonprio = data.stats_prio.get().time[0],
                timePrio = data.stats_prio.get().time[1]
            )
        }

        HorizontalDivider()

        MemberAdd(
            modifier = Modifier.padding(top = 5.dp),
            onAdd = { name, prio -> update(Event.CreateMember(name, prio)) }
        )
    }
}

@Composable
fun SpeakerBar(data: SessionView, update: (Event) -> Unit, modifier: Modifier = Modifier) {
    val speaker = data.active_speaker.getOrNull()
    val nextSpeaker = data.queue.getOrNull(0)

    val density = LocalDensity.current

    val nextSpeakerShape = GenericShape { size: Size, _: LayoutDirection ->
        val width = size.width
        val height = size.height

        val (diameter, adjust) = with(density) {
            listOf(50.dp.toPx(), (2.5).dp.toPx())
        }

        arcTo(
            rect = Rect(offset = Offset(-diameter * 0.6f, -adjust), size = Size(diameter, diameter)),
            startAngleDegrees = -90f,
            sweepAngleDegrees = 180f,
            forceMoveTo = false,
        )
        lineTo(width, height)

        arcTo(
            rect = Rect(offset = Offset(width - height, 0f), size = Size(height, height)),
            startAngleDegrees = 90f,
            sweepAngleDegrees = -180f,
            forceMoveTo = false,
        )

        close()
    }

    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy((-10).dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Row(
            modifier = Modifier
                .background(
                    color = MaterialTheme.colorScheme.secondaryContainer,
                    shape = RoundedCornerShape(percent = 100),
                )
                .height(50.dp),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            if (speaker != null) {
                if (data.timer_active) {
                    IconButton(onClick = { update(Event.StopTimer()) }) {
                        Icon(
                            painterResource(R.drawable.baseline_pause_24),
                            contentDescription = "Start/pause timer",
                            tint = MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    }
                } else {
                    IconButton(onClick = { update(Event.StartTimer()) }) {
                        Icon(
                            Icons.Filled.PlayArrow,
                            contentDescription = "Start/pause timer",
                            tint = MaterialTheme.colorScheme.onSecondaryContainer
                        )
                    }
                }
                Text(
                    if (speaker.length > 12) { speaker.take(12) + "…" } else { speaker },
                    overflow = TextOverflow.Ellipsis,
                    modifier = Modifier.padding(end = 15.dp),
                    fontSize = 23.sp,
                    color = MaterialTheme.colorScheme.onSecondaryContainer,
                )
            } else {
                Text(
                    "(Pause)",
                    modifier = Modifier.padding(start = 5.dp, end = 5.dp, bottom = 3.dp),
                    fontSize = 18.sp,
                    color = MaterialTheme.colorScheme.onSurfaceVariant,
                )
            }
        }

        if (speaker != null || nextSpeaker != null) {
            Row(
                modifier = Modifier
                    .height(45.dp)
                    .clip(nextSpeakerShape)
                    .background(color = MaterialTheme.colorScheme.tertiary)
                    .clickable { update(Event.NextSpeaker()) },
                verticalAlignment = Alignment.CenterVertically,
            ) {
                Spacer(modifier.width(25.dp))
                Icon(
                    Icons.AutoMirrored.Filled.ArrowForward,
                    contentDescription = "Next",
                    modifier = Modifier.size(18.dp),
                    tint = MaterialTheme.colorScheme.onTertiary
                )
                if (nextSpeaker != null) {
                    Text(
                        nextSpeaker,
                        overflow = TextOverflow.Ellipsis,
                        modifier = Modifier.padding(start = 3.dp),
                        fontSize = 18.sp,
                        color = MaterialTheme.colorScheme.onTertiary,
                    )
                } else {
                    Text(
                        "(Fin)",
                        modifier = Modifier.padding(start = 3.dp),
                        fontSize = 15.sp,
                        color = MaterialTheme.colorScheme.onTertiary,
                    )
                }
                Spacer(modifier.width(10.dp))
            }
        }
    }
}

@Composable
fun EditMembers(members: List<MemberView>, update: (Event) -> Unit) {
    MemberList(
        members,
        onPrioToggle = { idx -> update(Event.ToggleMemberPrio(idx)) },
        onHiddenToggle = { idx -> update(Event.ToggleMemberHidden(idx)) },
        onDelete = { idx -> update(Event.DeleteMember(idx)) },
        onAdd = Optional.of { name, prio -> update(Event.CreateMember(name, prio)) },
    )
}