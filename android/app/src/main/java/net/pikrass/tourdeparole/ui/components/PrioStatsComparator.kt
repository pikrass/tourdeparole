package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.Icon
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import net.pikrass.tourdeparole.R
import net.pikrass.tourdeparole.ui.theme.TourDeParoleTheme
import kotlin.math.roundToInt

@Composable
fun PrioStatsComparator(
    nbNonprio: Long,
    nbPrio: Long,
    speechesNonprio: Long,
    speechesPrio: Long,
    timeNonprio: Long,
    timePrio: Long,
    modifier: Modifier = Modifier,
) {
    ConstraintLayout(
        modifier = modifier,
    ) {
        val (
            bar,
            peoplePercents,
            speechesPercents,
            timePercents,

            peopleIcon,
            peopleNonprioText,
            peoplePrioText,
            speechesIcon,
            speechesNonprioText,
            speechesPrioText,
            speechesBar,
            timeIcon,
            timeNonprioText,
            timePrioText,
            timeBar,
        ) = createRefs()

        val iconBarrier = createEndBarrier(peopleIcon, speechesIcon, timeIcon, margin = 3.dp)
        val leftBarrier = createEndBarrier(peopleNonprioText, speechesNonprioText, timeNonprioText)
        val rightBarrier = createStartBarrier(peoplePrioText, speechesPrioText, timePrioText)

        val speechesBarrier = createBottomBarrier(peopleNonprioText, peoplePrioText, margin = (-7).dp)
        val timeBarrier = createBottomBarrier(speechesNonprioText, speechesBar, speechesPrioText, margin = (-7).dp)

        val iconSize = 14.dp
        val textSize = 12.sp
        val barTextSize = 10.sp
        val barMargin = 4.dp
        val barHeight = 10.dp

        Icon(
            Icons.Filled.Person,
            modifier = Modifier.size(iconSize).constrainAs(peopleIcon) {
                centerVerticallyTo(peopleNonprioText)
            },
            contentDescription = "People",
        )

        Icon(
            painterResource(R.drawable.baseline_mic_24),
            modifier = Modifier.size(iconSize).constrainAs(speechesIcon) {
                top.linkTo(speechesBarrier)
                centerVerticallyTo(speechesBar)
            },
            contentDescription = "Speeches",
        )

        Icon(
            painterResource(R.drawable.baseline_access_time_24),
            modifier = Modifier.size(iconSize).constrainAs(timeIcon) {
                top.linkTo(timeBarrier)
                centerVerticallyTo(timeBar)
            },
            contentDescription = "Time",
        )

        LinearProgressIndicator(
            modifier = Modifier.height(barHeight).constrainAs(speechesBar) {
                centerVerticallyTo(speechesNonprioText)
                top.linkTo(speechesBarrier)
                start.linkTo(leftBarrier, margin = barMargin)
                end.linkTo(rightBarrier, margin = barMargin)
            },
            progress = { speechesNonprio.toFloat() / (speechesNonprio + speechesPrio).toFloat() },
            trackColor = Color(red = 252, green = 140, blue = 20),
        )

        LinearProgressIndicator(
            modifier = Modifier.height(barHeight).constrainAs(timeBar) {
                centerVerticallyTo(timeNonprioText)
                top.linkTo(timeBarrier)
                start.linkTo(leftBarrier, margin = barMargin)
                end.linkTo(rightBarrier, margin = barMargin)
            },
            progress = { timeNonprio.toFloat() / (timeNonprio + timePrio).toFloat() },
            trackColor = Color(red = 252, green = 140, blue = 20),
        )

        Text(
            modifier = Modifier.constrainAs(peopleNonprioText) {
                start.linkTo(iconBarrier)
                end.linkTo(leftBarrier)
            },
            fontSize = textSize,
            text = nbNonprio.toString(),
        )

        Text(
            modifier = Modifier.constrainAs(peoplePrioText) {
                start.linkTo(rightBarrier)
                end.linkTo(parent.end)
            },
            fontSize = textSize,
            text = nbPrio.toString(),
        )

        Text(
            modifier = Modifier.constrainAs(speechesNonprioText) {
                top.linkTo(speechesBarrier)
                start.linkTo(iconBarrier)
                end.linkTo(leftBarrier)
            },
            fontSize = textSize,
            text = speechesNonprio.toString(),
        )

        Text(
            modifier = Modifier.constrainAs(speechesPrioText) {
                top.linkTo(speechesBarrier)
                start.linkTo(rightBarrier)
                end.linkTo(parent.end)
            },
            fontSize = textSize,
            text = speechesPrio.toString(),
        )

        Text(
            modifier = Modifier.constrainAs(timeNonprioText) {
                top.linkTo(timeBarrier)
                start.linkTo(iconBarrier)
            },
            fontSize = textSize,
            text = "%d:%02d".format(timeNonprio / 60000, (timeNonprio % 60000) / 1000),
        )

        Text(
            modifier = Modifier.constrainAs(timePrioText) {
                top.linkTo(timeBarrier)
                start.linkTo(rightBarrier)
                end.linkTo(parent.end)
            },
            fontSize = textSize,
            text = "%d:%02d".format(timePrio / 60000, (timePrio % 60000) / 1000),
        )

        PrioBar(
            modifier = Modifier.constrainAs(bar) {
                width = Dimension.fillToConstraints
                height = Dimension.fillToConstraints
                start.linkTo(leftBarrier)
                end.linkTo(rightBarrier)
                top.linkTo(parent.top, margin = 5.dp)
                bottom.linkTo(parent.bottom, margin = 2.dp)
            },
            nbNonprio = nbNonprio,
            nbPrio = nbPrio,
        )

        PrioPercents(
            modifier = Modifier.constrainAs(peoplePercents) {
                width = Dimension.fillToConstraints
                start.linkTo(leftBarrier)
                end.linkTo(rightBarrier)
            },
            numNonprio = nbNonprio,
            numPrio = nbPrio,
            fontSize = textSize,
        )

        PrioPercents(
            modifier = Modifier.constrainAs(speechesPercents) {
                width = Dimension.fillToConstraints
                start.linkTo(leftBarrier)
                end.linkTo(rightBarrier)
                top.linkTo(speechesBarrier)
            },
            numNonprio = speechesNonprio,
            numPrio = speechesPrio,
            fontSize = barTextSize,
            color = Color.White,
        )

        PrioPercents(
            modifier = Modifier.constrainAs(timePercents) {
                width = Dimension.fillToConstraints
                start.linkTo(leftBarrier)
                end.linkTo(rightBarrier)
                top.linkTo(timeBarrier)
            },
            numNonprio = timeNonprio,
            numPrio = timePrio,
            fontSize = barTextSize,
            color = Color.White,
        )
    }
}

@Composable
private fun PrioBar(
    nbNonprio: Long,
    nbPrio: Long,
    modifier: Modifier = Modifier,
) {
    Canvas(modifier = modifier.fillMaxSize()) {
        val x = size.width * nbNonprio.toFloat() / (nbNonprio + nbPrio).toFloat()
        drawLine(
            start = Offset(x = x, y = 0f),
            end = Offset(x = x, y = size.height),
            color = Color.Black,
            strokeWidth = 4f,
        )
    }
}

@Composable
private fun PrioPercents(
    numNonprio: Long,
    numPrio: Long,
    modifier: Modifier = Modifier,
    fontSize: TextUnit = TextUnit.Unspecified,
    color: Color = Color.Unspecified,
) {
    val left = numNonprio.toFloat() / (numNonprio + numPrio).toFloat()
    val right = 1f - left

    val displayLeft = (left * 100f).roundToInt()
    val displayRight = 100 - displayLeft

    Row(modifier = modifier) {
        if (displayLeft > 0) {
            Text(
                modifier = Modifier.weight(left),
                fontSize = fontSize,
                color = color,
                textAlign = TextAlign.Center,
                text = "$displayLeft%"
            )
        }
        if (displayRight > 0) {
            Text(
                modifier = Modifier.weight(right),
                fontSize = fontSize,
                color = color,
                textAlign = TextAlign.Center,
                text = "$displayRight%",
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun PrioStatsComparatorPreview() {
    TourDeParoleTheme {
        PrioStatsComparator(
            nbNonprio = 7,
            nbPrio = 4,
            speechesNonprio = 9,
            speechesPrio = 4,
            timeNonprio = 90000,
            timePrio = 55000,
        )
    }
}
