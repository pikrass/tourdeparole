@file:OptIn(ExperimentalMaterial3Api::class)

package net.pikrass.tourdeparole.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.OrderRuleData
import net.pikrass.tourdeparole.tdp_types.Profile
import net.pikrass.tourdeparole.ui.components.DurationPickerDialog
import net.pikrass.tourdeparole.ui.components.OrderingRules
import net.pikrass.tourdeparole.ui.components.ProfileItem
import kotlin.time.Duration.Companion.milliseconds

@Composable
fun ProfileList(profiles: List<Profile>, update: (Event) -> Unit) {
    LazyColumn(
        contentPadding = PaddingValues(horizontal = 8.dp, vertical = 16.dp),
        verticalArrangement = Arrangement.spacedBy(16.dp),
    ) {
        profiles.forEach { profile ->
            item {
                ProfileItem(profile, update)
            }
            item {
                HorizontalDivider()
            }
        }
    }
}

private enum class TimeEdit { NONE, FIRST, NEXT }

@Composable
fun EditProfile(profile: Profile, availOrderRules: List<OrderRuleData>, update: (Event) -> Unit) {
    var editTime by remember { mutableStateOf(TimeEdit.NONE) }

    if (editTime != TimeEdit.NONE) {
        DurationPickerDialog(
            initTime = if (editTime == TimeEdit.FIRST) {
                profile.timelimit_first.milliseconds
            } else {
                profile.timelimit_next.milliseconds
            },
            onCancel = {
                editTime = TimeEdit.NONE
            },
            onConfirm = { dur ->
                val limit = dur.inWholeMilliseconds
                when (editTime) {
                    TimeEdit.FIRST -> update(Event.EditProfileTimelimitFirst(profile.id, limit))
                    TimeEdit.NEXT -> update(Event.EditProfileTimelimitNext(profile.id, limit))
                    TimeEdit.NONE -> {}
                }
                editTime = TimeEdit.NONE
            },
        )
    }

    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(PaddingValues(horizontal = 8.dp, vertical = 16.dp)),
        verticalArrangement = Arrangement.spacedBy(16.dp),
    ) {
        ListItem(
            headlineContent = { Text("Nom du profil") },
            trailingContent = {
                TextField(
                    modifier = Modifier.width(150.dp),
                    value = profile.name,
                    onValueChange = { name ->
                        update(Event.EditProfileName(profile.id, name))
                    }
                )
            }
        )

        ListItem(
            modifier = Modifier.clickable(onClick = {
                editTime = TimeEdit.FIRST
            }),
            headlineContent = { Text("Durée première intervention") },
            trailingContent = {
                Text("%01d:%02d".format(profile.timelimit_first / 60000, (profile.timelimit_first % 60000) / 1000))
            },
        )

        ListItem(
            modifier = Modifier.clickable(onClick = {
                editTime = TimeEdit.NEXT
            }),
            headlineContent = { Text("Durée interventions suivantes") },
            trailingContent = {
                Text("%01d:%02d".format(profile.timelimit_next / 60000, (profile.timelimit_next % 60000) / 1000))
            },
        )

        ListItem(
            headlineContent = { Text("Règles", fontWeight = FontWeight.Bold) },
        )

        OrderingRules(
            profileId = profile.id,
            orderRules = profile.order_rules,
            availOrderRules = availOrderRules,
            update = update,
        )

        ListItem(
            headlineContent = { Text("Réinitialiser remet à zéro...", fontWeight = FontWeight.Bold) },
        )

        ListItem(
            headlineContent = { Text("... la limite du temps de parole") },
            trailingContent = {
                Switch(
                    checked = profile.reset_timelimit,
                    onCheckedChange = { update(Event.ToggleResetTimelimit(profile.id)) }
                )
            }
        )

        ListItem(
            headlineContent = { Text("... le compteur d'interventions") },
            trailingContent = {
                Switch(
                    checked = profile.reset_speeches,
                    onCheckedChange = { update(Event.ToggleResetSpeeches(profile.id)) }
                )
            }
        )

        ListItem(
            headlineContent = { Text("... le compteur de temps de parole") },
            trailingContent = {
                Switch(
                    checked = profile.reset_timer,
                    onCheckedChange = { update(Event.ToggleResetTimer(profile.id)) }
                )
            }
        )

        ListItem(
            headlineContent = { Text("... les statistiques collectives") },
            trailingContent = {
                Switch(
                    checked = profile.reset_stats,
                    onCheckedChange = { update(Event.ToggleResetStats(profile.id)) }
                )
            }
        )

        ListItem(
            headlineContent = { Text("Supprimer", fontWeight = FontWeight.Bold) },
            colors = ListItemDefaults.colors(
                containerColor = MaterialTheme.colorScheme.errorContainer,
                headlineColor = MaterialTheme.colorScheme.onErrorContainer,
            ),
            modifier = Modifier.clickable(onClick = {
                update(Event.DeleteProfile(profile.id))
            })
        )
    }
}