@file:OptIn(ExperimentalMaterial3Api::class)

package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ListItem
import androidx.compose.material3.LocalContentColor
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import net.pikrass.tourdeparole.R
import net.pikrass.tourdeparole.tdp_types.Event
import net.pikrass.tourdeparole.tdp_types.Profile
import java.util.Optional

@Composable
fun RoundIconButton(icon: ImageVector, desc: String, onClick: () -> Unit, emphasis: Boolean = false) {
    var butModifier = Modifier.border(
            width = 1.dp,
            color = MaterialTheme.colorScheme.outline,
            shape = RoundedCornerShape(percent = 100)
        )
    var tint = LocalContentColor.current

    if (emphasis) {
        butModifier = butModifier.background(
            color = MaterialTheme.colorScheme.primary,
            shape = RoundedCornerShape(percent = 100)
        )
        tint = MaterialTheme.colorScheme.onPrimary
    }

    IconButton(
        onClick = onClick,
        modifier = butModifier
    ) {
        Icon(icon, contentDescription = desc, tint = tint)
    }
}

@Composable
fun ProfileItem(profile: Profile, update: (Event) -> Unit) {
    ListItem(
        headlineContent = { Text(profile.name) },
        trailingContent = {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
            ) {
                RoundIconButton(
                    icon = Icons.Filled.Edit,
                    desc = "Editer",
                    onClick = {
                        update(Event.NavEditProfile(profile.id))
                    },
                )
                RoundIconButton(
                    icon = ImageVector.vectorResource(R.drawable.baseline_content_copy_24),
                    desc = "Dupliquer",
                    onClick = {
                        update(Event.CloneProfile(profile.id))
                    },
                )
                RoundIconButton(
                    icon = ImageVector.vectorResource(R.drawable.baseline_forum_24),
                    desc = "Démarrer",
                    onClick = {
                        update(Event.NewSession(Optional.of(profile.id)))
                    },
                    emphasis = true
                )
            }
        }
    )
}