package net.pikrass.tourdeparole.ui.components

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.Composable

@Composable
fun PrioButton(pressed: Boolean, onClick: () -> Unit) {
    ElevatedButton(
        onClick = onClick,
        elevation = ButtonDefaults.buttonElevation(
            defaultElevation = if (pressed) { 2.dp } else { 4.dp },
            pressedElevation = 1.dp,
        ),
        colors = if (pressed) {
            ButtonDefaults.elevatedButtonColors(
                containerColor = Color(red = 252, green = 140, blue = 20),
                contentColor = Color(red = 252, green = 213, blue = 20),
            )
        } else {
            ButtonDefaults.elevatedButtonColors(
                containerColor = Color(red = 220, green = 220, blue = 220),
                contentColor = ButtonDefaults.elevatedButtonColors().disabledContentColor,
            )
        },
        contentPadding = PaddingValues(0.dp),
    ) {
        Text(
            "!",
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold,
            style = TextStyle(
                shadow = Shadow(
                    offset = Offset(2f, 2f),
                    color = Color(red = 0, green = 0, blue = 0, alpha = 120)
                )
            )
        )
    }
}