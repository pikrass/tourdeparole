plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("org.mozilla.rust-android-gradle.rust-android")
}

android {
    namespace = "net.pikrass.tourdeparole.tdp_logic"
    compileSdk = 33

    ndkVersion = "26.1.10909125"

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    sourceSets.getByName("main") {
        java.srcDir("${projectDir}/../../tdp_types/generated/java")
    }
}

dependencies {
    implementation("net.java.dev.jna:jna:5.13.0@aar")
    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}

apply(plugin = "org.mozilla.rust-android-gradle.rust-android")

cargo {
    module = "../.."
    libname = "tdp_logic"
    targets = listOf("arm", "arm64", "x86", "x86_64")
    extraCargoBuildArguments = listOf("--package", "tdp_logic")
}

afterEvaluate {
    android.libraryVariants.configureEach {
        var productFlavor = ""
        this.productFlavors.forEach {
            productFlavor += "${it.name.capitalize()}"
        }
        var buildType = "${this.buildType.name.capitalize()}"

        tasks.named("compileDebugKotlin") {
            this.dependsOn(tasks.named("typesGen"), tasks.named("bindGen"))
        }

        tasks.named("merge${productFlavor}${buildType}JniLibFolders") {
            this.inputs.dir(File(buildDir, "rustJniLibs/android"))
            this.dependsOn(tasks.named("cargoBuild"))
        }
    }
}

tasks.register<Exec>("bindGen") {
    var outDir = "${projectDir}/../../tdp_types/generated/java"
    this.workingDir("../..")

    if (System.getProperty("os.name").toLowerCase().contains("windows")) {
        this.commandLine(
            "cmd",
            "/c",
            "cargo build -p tdp_logic && " +
                    "target\\\\debug\\\\uniffi-bindgen generate tdp_logic\\\\src\\\\tdp_logic.udl" +
                    "--language kotlin --out-dir " + outDir.replace('/', '\\'))
    } else {
        this.commandLine(
            "sh",
            "-c",
            """\
                cargo build -p tdp_logic && \
                target/debug/uniffi-bindgen generate tdp_logic/src/tdp_logic.udl \
                --language kotlin \
                --out-dir $outDir
            """.trimIndent()
        )
    }
}

tasks.register<Exec>("typesGen") {
    this.workingDir("../..")
    if (System.getProperty("os.name").toLowerCase().contains("windows")) {
        this.commandLine("cmd", "/c", "cargo build -p tdp_types")
    } else {
        this.commandLine("sh", "-c", "cargo build -p tdp_types")
    }
}
